Development
===========

Installation
------------

The library can be installed using ``pip`` but you need to make sure to use the
SKA artefact repository as the index:

.. code-block:: bash

    python -m pip install ska-sdp-scripting \
    --index-url https://artefact.skao.int/repository/pypi-all/simple

To install from source, clone the repo and install the dependencies via
`poetry <https://python-poetry.org/docs/basic-usage/>`_:

.. code-block:: bash

    git clone https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting --recurse-submodules


Develop a new processing script
-------------------------------

The steps to develop and test an SDP processing script can be found at
:doc:`Script Development <ska-sdp-script:script-development>`.
