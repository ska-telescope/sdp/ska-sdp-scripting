SDP Scripting Library
=====================

The SDP scripting library is a high-level interface for writing processing
scripts. Its goal is to provide abstractions to enable developers to express the
high-level organisation and functionality of a processing script without needing
to interact directly with any of the low-level interfaces such as the SDP
configuration library.

.. toctree::
  :maxdepth: 1

  development
  functionality
  port-configuration
  api

Indices and tables
------------------

- :ref:`genindex`
