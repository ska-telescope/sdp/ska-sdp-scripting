# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

from pathlib import Path
import sys

sys.path.insert(0, str(Path("../../src").absolute()))

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "SDP Scripting Library"
copyright = "2019-2025 SKA SDP Developers"
author = "SKA SDP Developers"
version = "1.0.0"
release = "1.0.0"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.intersphinx",
    "sphinx_copybutton",
    "sphinx_paramlinks",
]

exclude_patterns = []

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "ska_ser_sphinx_theme"

# -- Extension configuration -------------------------------------------------

autodoc_mock_imports = [
    "distributed",
    "ska_sdp_config",
    "ska_ser_logging",
    "ska_telmodel",
]

# Intersphinx configuration
intersphinx_mapping = {
    "ska-sdp-script": ("https://developer.skao.int/projects/ska-sdp-script/en/latest", None),
    "ska-telmodel": ("https://developer.skao.int/projects/ska-telmodel/en/latest", None),
}
