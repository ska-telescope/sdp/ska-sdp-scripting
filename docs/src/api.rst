API
===

.. _procblock:

Processing block
----------------

.. autoclass:: ska_sdp_scripting.processing_block.ProcessingBlock
   :members:
   :undoc-members:

Buffer request
--------------

.. autoclass:: ska_sdp_scripting.buffer_request.BufferRequest
   :members:
   :undoc-members:

Script phase
------------

.. autoclass:: ska_sdp_scripting.phase.Phase
   :members:
   :undoc-members:

Execution Engine (EE) deployment
--------------------------------

.. autoclass:: ska_sdp_scripting.ee_base_deploy.EEDeploy
   :members:
   :undoc-members:

Helm EE Deployment
------------------

.. autoclass:: ska_sdp_scripting.helm_deploy.HelmDeploy
   :members:
   :undoc-members:

Slurm EE Deployment
-------------------

.. autoclass:: ska_sdp_scripting.slurm_deploy.SlurmDeploy
   :members:
   :undoc-members:

Dask EE deployment
------------------

.. autoclass:: ska_sdp_scripting.dask_deploy.DaskDeploy
   :members:
   :undoc-members:

Fake EE deployment
------------------

.. autoclass:: ska_sdp_scripting.fake_deploy.FakeDeploy
   :members:
   :undoc-members:

Queue connector functions
-------------------------
.. automodule:: ska_sdp_scripting.queue_connector
   :members:
   :undoc-members:
   :show-inheritance:

Receive Addresses utilities
---------------------------
.. automodule:: ska_sdp_scripting.receive_addresses
   :members:
   :undoc-members:
   :show-inheritance:

