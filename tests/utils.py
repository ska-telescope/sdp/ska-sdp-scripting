"""Testing utils"""

import json
from datetime import datetime
from pathlib import Path

import ska_sdp_config

SUBARRAY_ID = "01"
PB_ID = "pb-test-20200425-00000"


def create_work_phase(pb, flows=None):
    """Create work phase."""
    in_buffer_res = pb.request_buffer(100e6, tags=["sdm"])
    out_buffer_res = pb.request_buffer(10 * 6e15 / 3600, tags=["visibilities"])

    for flow in flows or []:
        pb.create_data_flow(**flow)

    return pb.create_phase("Work", [in_buffer_res, out_buffer_res])


def create_eb_pb(db_client, inconsistent_beam=False):
    """Create execution block and processing block."""
    eb, eb_state, pbs = get_eb_pbs(inconsistent_beam=inconsistent_beam)
    for txn in db_client.txn():
        eb_id = eb.key
        if eb_id is not None:
            txn.execution_block.create(eb)
            txn.execution_block.state(eb_id).create(eb_state)
        for pb in pbs:
            txn.processing_block.create(pb)


def get_eb_pbs(inconsistent_beam=False):
    """Get EB and PBs from configuration string."""
    config = read_configuration_string(inconsistent_beam=inconsistent_beam)

    pbs_from_config = config.pop("processing_blocks")
    eb_id = config.get("eb_id")
    config.update({"key": eb_id, "subarray_id": SUBARRAY_ID})

    eb = ska_sdp_config.entity.ExecutionBlock(**config)

    eb_state = {
        "scan_type": None,
        "scan_id": None,
        "status": "ACTIVE",
    }

    pbs = []
    for pbc in pbs_from_config:
        pb_id = pbc.get("pb_id")
        kind = pbc.get("script").get("kind")
        # Only add batch and realtime pbs
        if realtime_or_batch := getattr(eb, f"pb_{kind}"):
            realtime_or_batch.append(pb_id)

        dependencies = pbc.get("dependencies", [])
        pb = ska_sdp_config.entity.ProcessingBlock(
            key=pb_id,
            eb_id=eb_id,
            script=pbc.get("script"),
            parameters=pbc.get("parameters"),
            dependencies=dependencies,
        )
        pbs.append(pb)

    return eb, eb_state, pbs


def read_configuration_string(inconsistent_beam=False):
    """Read configuration string from JSON file."""
    config = read_json_data("configuration_string.json", decode=True)
    if inconsistent_beam:
        config["scan_types"].append(
            {
                "scan_type_id": "target:b",
                "derive_from": ".default",
                "beams": {"vis0": {"field_id": "field_a"}},
            }
        )

    return config


def read_json_data(filename, decode=False):
    """Read JSON file from data directory.

    :param decode: decode the JSON dat into Python

    """
    path = Path(__file__).parent / "data" / filename
    with path.open("r", encoding="utf8") as file:
        data = file.read()
    if decode:
        data = json.loads(data)
    return data


def create_pb_states(config_db_client):
    """
    Create PB states in the config DB.

    This creates the PB states with status = RUNNING, and for any script
    matching the list of receive scripts, it adds the receive addresses.
    """

    for txn in config_db_client.txn():
        pb_list = txn.processing_block.list_keys()
        for pb_id in pb_list:
            pb_state = txn.processing_block.state(pb_id).get()
            if pb_state is None:
                txn.processing_block.state(pb_id).create(
                    {"resources_available": "True", "status": "RUNNING"}
                )


def wait_for_deployments(work_phase, deploy):
    """
    Wait for deployments to appear in db
    """
    start = datetime.now()
    # pylint: disable-next=protected-access
    for watcher in work_phase._config.watcher():
        for txn in watcher.txn():
            deploy_id = deploy.deploy_id
            deployment_list = txn.deployment.list_keys()
        if deploy_id in deployment_list:
            break
        if (datetime.now() - start).seconds > 2:
            raise AssertionError("deploy_id is not in deployment list")
