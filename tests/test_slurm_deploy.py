# pylint: disable=protected-access

"""
Tests for the slurm_deploy.py code
"""

from ska_sdp_scripting.utils import DeploymentStatus
from tests.utils import PB_ID


def test_deployment_is_pending(work_phase):
    """
    When a deployment is being created, its
    status in the processing block state should
    always start with PENDING
    """
    deploy_name = "test-slurm-deployer"
    deploy_id = f"proc-{PB_ID}-{deploy_name}"

    with work_phase:
        for txn in work_phase._config.txn():
            work_phase.ee_deploy_slurm(deploy_name)

            pb_state = txn.processing_block.state(PB_ID).get()
            assert (
                pb_state["deployments"][deploy_id]
                == DeploymentStatus.PENDING.value
            )


def test_create_deployment(work_phase):
    """
    Test to check if the deployment was created with
    the correct arguments, kind etc
    """
    deploy_name = "test-slurm-deployer"
    deploy_id = f"proc-{PB_ID}-{deploy_name}"

    # Slurm Arguments
    slargs = {
        "script": "#!/bin/bash\n\n echo Hello Slurm;",
        "tasks": 20,
        "nodes": 2,
        "current_working_directory": "test_path",
    }

    with work_phase:
        for txn in work_phase._config.txn():
            work_phase.ee_deploy_slurm(deploy_name, slargs)

            result = txn.deployment.get(deploy_id)
            assert result.kind == "slurm"
            assert result.args == slargs
