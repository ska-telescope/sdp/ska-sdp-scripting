# pylint: disable=protected-access

"""SDP Scripting library integration tests."""
import os
from unittest.mock import patch

from ska_sdp_scripting import ProcessingBlock
from ska_sdp_scripting.utils import (
    DeploymentStatus,
    ExecutionBlockStatus,
    ProcessingBlockStatus,
)
from tests.utils import (
    PB_ID,
    create_eb_pb,
    create_pb_states,
    wait_for_deployments,
)

MOCK_ENV_VARS = {"SDP_HELM_NAMESPACE": "sdp"}


def test_claim_processing_block(config_db_client):
    """Test claiming processing block"""
    with patch(
        "ska_sdp_scripting.processing_block.new_config_client"
    ) as mock_client:
        mock_client.return_value = config_db_client
        # Create eb and pb
        create_eb_pb(config_db_client)

        for txn in config_db_client.txn():
            pb_list = txn.processing_block.list_keys()

        for pb_id in pb_list:
            for txn in config_db_client.txn():
                assert txn.processing_block.get(pb_id).key == pb_id
                ProcessingBlock(pb_id)
            for txn in config_db_client.txn():
                assert txn.processing_block.ownership(
                    pb_id
                ).is_owned_by_this_process()


def test_real_time_script(work_phase):
    """Test real time script."""
    deploy_name = "cbf-sdp-emulator-test"
    deploy_id = f"proc-{PB_ID}-{deploy_name}"

    with work_phase:
        for txn in work_phase._config.txn():
            eb_list = txn.execution_block.list_keys()

        for eb_id in eb_list:
            for txn in work_phase._config.txn():
                eb = txn.execution_block.state(eb_id).get()
                status = eb.get("status")
                assert status == ExecutionBlockStatus.ACTIVE.value

                pb_state = txn.processing_block.state(PB_ID).get()
                pb_status = pb_state.get("status")
                assert pb_status == ProcessingBlockStatus.RUNNING.value

                work_phase.ee_deploy_helm(deploy_name)
                deployment_list = txn.deployment.list_keys()
                assert deploy_id in deployment_list

            for txn in work_phase._config.txn():
                # Set processing block to READY
                work_phase.update_pb_state(status=ProcessingBlockStatus.READY)
                p_state = txn.processing_block.state(PB_ID).get()
                p_status = p_state.get("status")
                assert p_status == ProcessingBlockStatus.READY.value

                # Set execution block to FINISHED
                eb = {
                    "status": ExecutionBlockStatus.FINISHED.value,
                }
                eb_state = txn.execution_block.state(eb_id).get()
                eb_state.update(eb)
                txn.execution_block.state(eb_id).update(eb_state)

                eb = txn.execution_block.state(eb_id).get()
                status = eb.get("status")
                assert status == ExecutionBlockStatus.FINISHED.value

    for txn in work_phase._config.txn():
        pb_state = txn.processing_block.state(PB_ID).get()
        pb_status = pb_state.get("status")
        assert pb_status == ProcessingBlockStatus.FINISHED.value


@patch.dict(os.environ, MOCK_ENV_VARS)
def test_batch_script(work_phase):
    """Test batch script"""

    def calc(x, y):
        x1 = x
        y1 = y
        z = x1 + y1
        return z

    create_pb_states(work_phase._config)

    deploy_name = "dask"
    n_workers = 2

    with work_phase:
        for txn in work_phase._config.txn():
            pb_state = txn.processing_block.state(PB_ID).get()
            pb_status = pb_state.get("status")
            assert pb_status == ProcessingBlockStatus.RUNNING.value

        deploy = work_phase.ee_deploy_dask(
            deploy_name, n_workers, calc, (1, 5)
        )

        wait_for_deployments(work_phase, deploy)
        deploy_id = deploy.deploy_id

        for txn in work_phase._config.txn():
            state = txn.processing_block.state(PB_ID).get()
            deployments = state.get("deployments")
            deployments[deploy_id] = DeploymentStatus.FINISHED.value
            state["deployments"] = deployments
            txn.processing_block.state(PB_ID).update(state)

    for txn in work_phase._config.txn():
        pb_state = txn.processing_block.state(PB_ID).get()
        pb_status = pb_state.get("status")
        assert pb_status == ProcessingBlockStatus.FINISHED.value
