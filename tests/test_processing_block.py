# pylint: disable=protected-access
"""
Unit tests for ProcessingBlock methods
"""
from typing import Annotated
from unittest.mock import patch

import pytest
from pydantic import ConfigDict, Field, ValidationError

from ska_sdp_scripting import ProcessingBlock
from ska_sdp_scripting.processing_block import ParameterBaseModel
from tests.utils import (
    PB_ID,
    create_eb_pb,
    create_pb_states,
    read_configuration_string,
)


class TestDaskParams(ParameterBaseModel):
    """
    test-dask script parameters. Replicated from
    "https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/blob/master/
    src/ska-sdp-script-test-dask/pb_params.py?ref_type=heads"
    for testing parameter validation
    """

    model_config = ConfigDict(title="test-dask")

    n_workers: Annotated[
        int,
        Field(default=2),
    ]


def test_get_scan_types(pb):
    """
    Test that scan_types are correctly updated using
    default_scan_types and channel information
    """
    config = read_configuration_string(inconsistent_beam=True)
    channels = config.get("channels")

    # check that the scan_type which derives from a pseudo (default) scan type
    # doesn't yet contain the channel information from the default scan type
    default_scan_type = config.get("scan_types")[0]
    original_scan_type = config.get("scan_types")[1]
    assert original_scan_type.get("scan_type_id") == "target:a"
    assert original_scan_type.get("beams") != default_scan_type.get("beams")

    # the beam called 'vis0' in target:0 will be updated with
    # channel information and the information from the default_scan_type
    # for vis0 it will also have it's original information
    # NOTE: these are matched based on criteria (see code),
    # not all beam types will have all the channels
    expected_vis0_beam = {
        **original_scan_type.get("beams")["vis0"],
        **default_scan_type.get("beams")["vis0"],
        **channels[0],
    }

    # now we update scan_type target:a
    result_scan_types = pb.get_scan_types()

    assert (
        len(result_scan_types) == 1
    )  # we removed the default scan type and updated the original one
    assert result_scan_types[0].get("scan_type_id") == "target:a"
    assert result_scan_types[0].get("beams")["vis0"] == expected_vis0_beam


def test_get_dependency(config_db_client):
    """
    Return the expected dependencies defined in configuration_string.json
    """
    with patch(
        "ska_sdp_scripting.processing_block.new_config_client"
    ) as mock_client:
        mock_client.return_value = config_db_client
        create_eb_pb(config_db_client)

        pb_id_dep_map = {
            "pb-test-20200425-00000": [],
            "pb-test-20200425-00001": ["pb-test-20200425-00000"],
            "pb-test-20200425-00002": [
                "pb-test-20200425-00000",
                "pb-test-20200425-00001",
            ],
        }

        for txn in config_db_client.txn():
            pb_list = txn.processing_block.list_keys()

            for pb_id in pb_list:
                pb = ProcessingBlock(pb_id)

                dependencies = pb.get_dependencies()

                assert len(dependencies) == len(pb_id_dep_map[pb_id])
                dep_ids = [dep.pb_id for dep in dependencies]
                assert dep_ids == pb_id_dep_map[pb_id]


def test_pb_parameter_validation(config_db_client):
    """
    Processing Block parameters are correctly validated
    against a given Pydantic model.

    By default, we create a processing block of test_dask,
    which has the id of "pb-test-20200425-00002" in
    tests/data/configuration_string.json
    """
    with patch(
        "ska_sdp_scripting.processing_block.new_config_client"
    ) as mock_client:
        mock_client.return_value = config_db_client
        create_eb_pb(config_db_client)
        pb_id = "pb-test-20200425-00002"
        pb = ProcessingBlock(pb_id)

        # since parameters are correct, this does not raise an error
        result = pb.validate_parameters(model=TestDaskParams)
        assert isinstance(result, TestDaskParams)
        assert result.n_workers == 3


@pytest.mark.parametrize("wrong_type", ["a", ["a", "b"], [1, 2], (1, 2), 12.0])
def test_pb_parameter_validation_bad_arg_type(wrong_type, config_db_client):
    """
    Processing Block parameters contain the right key but
    incorrect type.

    By default, we create a processing block of test_dask,
    which has the id of "pb-test-20200425-00002" in
    tests/data/configuration_string.json
    """
    with patch(
        "ska_sdp_scripting.processing_block.new_config_client"
    ) as mock_client:
        mock_client.return_value = config_db_client
        create_eb_pb(config_db_client)
        pb_id = "pb-test-20200425-00002"

        for txn in config_db_client.txn():
            pb_in_db = txn.processing_block.get(pb_id)
            pb_in_db.parameters = {"n_workers": wrong_type}
            txn.processing_block.update(pb_in_db)

        pb = ProcessingBlock(pb_id)

        # since parameters are incorrect, we get ValidationError
        with pytest.raises(ValidationError):
            pb.validate_parameters(model=TestDaskParams)


def test_pb_parameter_validation_bad_key(config_db_client):
    """
    Processing Block parameters contain incorrect keys.

    By default, we create a processing block of test_dask,
    which has the id of "pb-test-20200425-00002" in
    tests/data/configuration_string.json
    """
    bad_keys = ["some_key", "some_other_key"]
    with patch(
        "ska_sdp_scripting.processing_block.new_config_client"
    ) as mock_client:
        mock_client.return_value = config_db_client
        create_eb_pb(config_db_client)
        pb_id = "pb-test-20200425-00002"

        for txn in config_db_client.txn():
            pb_in_db = txn.processing_block.get(pb_id)
            pb_in_db.parameters = {
                "n_workers": 1,
                bad_keys[0]: "not-allowed",
                bad_keys[1]: 2,
            }
            txn.processing_block.update(pb_in_db)

        pb = ProcessingBlock(pb_id)

        # since parameters are incorrect, we get ValidationError
        with pytest.raises(ValidationError) as err:
            pb.validate_parameters(model=TestDaskParams)

        error_msg = str(err.value).split("\n")
        assert "2 validation errors for test-dask" in error_msg
        assert bad_keys[0] in error_msg
        assert bad_keys[1] in error_msg


def test_pb_exit_exception(pb):
    """
    Test that exceptions within scripts using the
    ProcessingBlock class are handled and that the
    pb state is updated with the error information.
    """

    def artificial_error():
        raise ValueError("Artificial error for test")

    with patch.object(pb._config, "close", return_value=None):
        create_pb_states(pb._config)

        with pytest.raises(ValueError):
            with pb:
                artificial_error()

        for txn in pb._config.txn():
            pb_state = txn.processing_block.state(PB_ID).get()

            assert "script_error" in pb_state["error_messages"]
            error_message = pb_state["error_messages"]["script_error"]
            assert error_message["type"] == "ValueError"
            assert error_message["message"] == "Artificial error for test"
            assert "artificial_error" in error_message["traceback"]

        # pylint: disable=protected-access
        pb._handle_exception(exc_type=None, exc_val=None, exc_tb=None)

        for txn in pb._config.txn():
            pb_state = txn.processing_block.state(PB_ID).get()

            assert "script_error" not in pb_state["error_messages"]
