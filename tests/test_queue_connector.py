"""
Tests for the queue_connector.py code
"""

import time
from datetime import datetime
from threading import Thread

import pytest
from ska_sdp_config.config import Config
from ska_sdp_config.entity.flow import DataQueue, Flow, FlowSource

from ska_sdp_scripting.config import FEATURE_CONFIG_DB
from ska_sdp_scripting.utils import FlowStatus
from src.ska_sdp_scripting.queue_connector import (
    wait_for_queue_connector_state,
)


@pytest.mark.skipif(
    not FEATURE_CONFIG_DB.is_active(),
    reason="Watcher doesn't work well with memory backend",
)
@pytest.mark.timeout(3)
@pytest.mark.parametrize(
    "current_state, waiting_state",
    [
        (FlowStatus.FLOWING, None),
        (FlowStatus.FAULT, None),
        (FlowStatus.PENDING, FlowStatus.FLOWING.value),
        (FlowStatus.FAULT, FlowStatus.FLOWING.value),
    ],
)
def test_wait_for_queue_connector_state(
    config_db_client: Config,
    current_state: str,
    waiting_state: str | None,
):
    """
    Test that wait_for_queue_connector_state correctly returns
    True when state is changed to the expected.
    """
    pb_id = "pb-test-20200425-00001"
    flow_state = {"status": current_state.value}

    test_flow = Flow(
        key=Flow.Key(pb_id=pb_id, name="test-flow"),
        data_model="PointingTable",
        sink=DataQueue(
            host="kafka://localhost:9092", topics="test-topic", format="json"
        ),
        sources=[
            FlowSource(
                uri="http://test.com",
                function="ska-sdp-lmc-queue-connector:exchange",
            )
        ],
    )

    # Create QC flows
    for txn in config_db_client.txn():
        txn.flow.create(test_flow)
        txn.flow.state(test_flow.key).create(flow_state)

    def update_after_delay():
        time.sleep(1)
        new_state = {"status": waiting_state}
        for txn in config_db_client.txn():
            txn.flow.state(test_flow.key).update(new_state)

    # Set up thread to update key after delay
    thread = Thread(target=update_after_delay)

    start = datetime.now()
    thread.start()

    wait_for_queue_connector_state(config_db_client, pb_id, waiting_state, 10)

    # Make sure to wait for thread to finish
    thread.join()
    finish = datetime.now() - start

    # the update happens 1s after start, but
    # the whole process shouldn't take much more than that
    assert finish.total_seconds() >= 1
    assert finish.total_seconds() < 1.5


@pytest.mark.skipif(
    not FEATURE_CONFIG_DB.is_active(),
    reason="Watcher doesn't work well with memory backend",
)
@pytest.mark.parametrize(
    "current_state, waiting_state",
    [
        (FlowStatus.FLOWING, None),
        (FlowStatus.FAULT, None),
        (FlowStatus.PENDING, FlowStatus.FLOWING.value),
        (FlowStatus.FAULT, FlowStatus.FLOWING.value),
    ],
)
def test_wait_for_queue_connector_state_error(
    config_db_client: Config,
    current_state: str,
    waiting_state: str,
):
    """
    Test that wait_for_queue_connector_state raises
    timeout error when state is not reached
    """
    pb_id = "pb-test-20200425-00001"
    flow_state = {"status": current_state.value}

    for txn in config_db_client.txn():
        key = Flow.Key(pb_id=pb_id, name="testflow")
        txn.flow.create(
            Flow(
                key=key,
                sink=DataQueue(
                    topics="testtopic", host="kafka://localhost", format="json"
                ),
                sources=[
                    FlowSource(
                        uri="http://example.com",
                        function="ska-sdp-lmc-queue-connector:exchange",
                    )
                ],
                data_model="PointingTable",
            )
        )
        txn.flow.state(key).create(flow_state)

    with pytest.raises(TimeoutError):
        wait_for_queue_connector_state(
            config_db_client, pb_id, waiting_state, 4
        )
