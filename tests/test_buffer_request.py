"""Buffer request tests."""

from unittest.mock import patch

from ska_sdp_scripting import ProcessingBlock
from tests.utils import create_eb_pb


def test_buffer_request(config_db_client):
    """Test requesting input and output buffer."""
    with patch(
        "ska_sdp_scripting.processing_block.new_config_client"
    ) as mock_client:
        mock_client.return_value = config_db_client
        # Create eb and pb
        create_eb_pb(config_db_client)
        pb_list = ["pb-test-20200425-00000", "pb-test-20200425-00001"]

        for pb_id in pb_list:
            pb = ProcessingBlock(pb_id)
            parameters = pb.get_parameters()
            assert parameters["length"] == 10
            in_buffer_res = pb.request_buffer(100e6, tags=["sdm"])
            out_buffer_res = pb.request_buffer(
                parameters["length"] * 6e15 / 3600, tags=["visibilities"]
            )
            assert in_buffer_res is not None
            assert out_buffer_res is not None
