# pylint: disable=protected-access

"""
Tests for receive addresses and related code
"""

import os
from copy import deepcopy
from unittest.mock import patch

import pytest

from ska_sdp_scripting import ProcessingBlock
from ska_sdp_scripting.receive_addresses import generate_host_port_per_beam
from tests.utils import (
    PB_ID,
    create_eb_pb,
    create_pb_states,
    read_json_data,
    wait_for_deployments,
)

MOCK_ENV_VARS = {"SDP_HELM_NAMESPACE": "sdp"}

VALUES = {
    "reception.receiver_port_start": "9000",
    "reception.num_ports": 1,
}

SCAN_TYPES = [
    {
        "scan_type_id": "target:a",
        "derive_from": ".default",
        "beams": {
            "vis0": {
                "field_id": "field_a",
                "channels_id": "vis_channels",
                "polarisations_id": "all",
                "spectral_windows": [
                    {
                        "spectral_window_id": "fsp_1_channels",
                        "count": 4,
                        "start": 0,
                        "stride": 2,
                        "freq_min": 350000000,
                        "freq_max": 368000000,
                        "link_map": [[0, 0], [200, 1], [744, 2], [944, 3]],
                    },
                    {
                        "spectral_window_id": "fsp_2_channels",
                        "count": 7,
                        "start": 2000,
                        "stride": 1,
                        "freq_min": 360000000,
                        "freq_max": 368000000,
                        "link_map": [[2000, 4], [2200, 5]],
                    },
                ],
            }
        },
    },
    {
        "scan_type_id": "target:b",
        "derive_from": ".default",
        "beams": {
            "vis0": {
                "field_id": "field_a",
                "channels_id": "vis_channels",
                "polarisations_id": "all",
                "spectral_windows": [
                    {
                        "spectral_window_id": "fsp_1_channels",
                        "count": 4,
                        "start": 0,
                        "stride": 2,
                        "freq_min": 350000000,
                        "freq_max": 368000000,
                        "link_map": [[0, 0], [200, 1], [744, 2], [944, 3]],
                    },
                    {
                        "spectral_window_id": "fsp_2_channels",
                        "count": 4,
                        "start": 2000,
                        "stride": 1,
                        "freq_min": 360000000,
                        "freq_max": 368000000,
                        "link_map": [[2000, 4], [2200, 5]],
                    },
                ],
            }
        },
    },
]

# scan types returned by pb.get_scan_types() when
# configuration_string.json file is used
EXPECTED_SCAN_TYPES = [
    {
        "beams": {
            "vis0": {
                "field_id": "field_a",
                "channels_id": "vis_channels",
                "polarisations_id": "all",
                "spectral_windows": [
                    {
                        "count": 4,
                        "freq_max": 368000000.0,
                        "freq_min": 350000000.0,
                        "link_map": [[0, 0], [200, 1], [744, 2], [944, 3]],
                        "spectral_window_id": "fsp_1_channels",
                        "start": 0,
                        "stride": 2,
                    }
                ],
            },
            "pss1": {
                "channels_id": "pulsar_channels",
                "field_id": "pss_field_0",
                "polarisations_id": "all",
                "spectral_windows": [
                    {
                        "count": 2,
                        "freq_max": 368000000.0,
                        "freq_min": 350000000.0,
                        "spectral_window_id": "pulsar_fsp_channels",
                        "start": 0,
                        "stride": 2,
                    }
                ],
            },
            "pss2": {
                "field_id": "pss_field_1",
                "channels_id": "pulsar_channels",
                "polarisations_id": "all",
                "spectral_windows": [
                    {
                        "count": 2,
                        "freq_max": 368000000.0,
                        "freq_min": 350000000.0,
                        "spectral_window_id": "pulsar_fsp_channels",
                        "start": 0,
                        "stride": 2,
                    }
                ],
            },
            "pst1": {
                "field_id": "pst_field_0",
                "channels_id": "pulsar_channels",
                "polarisations_id": "all",
                "spectral_windows": [
                    {
                        "count": 2,
                        "freq_max": 368000000.0,
                        "freq_min": 350000000.0,
                        "spectral_window_id": "pulsar_fsp_channels",
                        "start": 0,
                        "stride": 2,
                    }
                ],
            },
            "pst2": {
                "field_id": "pst_field_1",
                "channels_id": "pulsar_channels",
                "polarisations_id": "all",
                "spectral_windows": [
                    {
                        "count": 2,
                        "freq_max": 368000000.0,
                        "freq_min": 350000000.0,
                        "spectral_window_id": "pulsar_fsp_channels",
                        "start": 0,
                        "stride": 2,
                    }
                ],
            },
            "vlbi1": {
                "field_id": "vlbi_field",
                "channels_id": "vlbi_channels",
                "polarisations_id": "all",
                "spectral_windows": [
                    {
                        "count": 2,
                        "freq_max": 368000000.0,
                        "freq_min": 350000000.0,
                        "spectral_window_id": "vlbi_fsp_channels",
                        "start": 0,
                        "stride": 2,
                    }
                ],
            },
        },
        "derive_from": ".default",
        "scan_type_id": "target:a",
    }
]

# receive addresses when SCAN_TYPE global variable is used
RECV_ADDRESS = {
    "target:a": {
        "vis0": {
            "host": [
                [
                    0,
                    "proc-pb-test-20200425-00000-test-receive-00-0."
                    "receive.sdp",
                ],
                [
                    2000,
                    "proc-pb-test-20200425-00000-test-receive-01-0."
                    "receive.sdp",
                ],
            ],
            "port": [[0, 9000], [2000, 9000]],
            "function": "visibilities",
        }
    },
    "target:b": {
        "vis0": {
            "host": [
                [
                    0,
                    "proc-pb-test-20200425-00000-test-receive-00-0."
                    "receive.sdp",
                ],
                [
                    2000,
                    "proc-pb-test-20200425-00000-test-receive-01-0."
                    "receive.sdp",
                ],
            ],
            "port": [[0, 9000], [2000, 9000]],
            "function": "visibilities",
        }
    },
}

# These are the receive addresses generated when
# the configuration_string.json file is used for scan_types
RECV_ADDR_FROM_CONFIG_STRING = {
    "target:a": {
        "vis0": {
            "host": [
                [
                    0,
                    "proc-pb-test-20200425-00000-test-receive-00-0."
                    "receive.sdp",
                ]
            ],
            "port": [[0, 9000]],
            "function": "visibilities",
        },
        "pss1": {
            "host": [
                [
                    0,
                    "proc-pb-test-20200425-00000-test-receive-00-0."
                    "receive.sdp",
                ]
            ],
            "port": [[0, 9000]],
            "function": "pulsar search",
            "search_beam_id": 1,
        },
        "pss2": {
            "host": [
                [
                    0,
                    "proc-pb-test-20200425-00000-test-receive-00-0."
                    "receive.sdp",
                ]
            ],
            "port": [[0, 9000]],
            "function": "pulsar search",
            "search_beam_id": 2,
        },
        "pst1": {
            "host": [
                [
                    0,
                    "proc-pb-test-20200425-00000-test-receive-00-0."
                    "receive.sdp",
                ]
            ],
            "port": [[0, 9000]],
            "function": "pulsar timing",
            "timing_beam_id": 1,
        },
        "pst2": {
            "host": [
                [
                    0,
                    "proc-pb-test-20200425-00000-test-receive-00-0."
                    "receive.sdp",
                ]
            ],
            "port": [[0, 9000]],
            "function": "pulsar timing",
            "timing_beam_id": 2,
        },
        "vlbi1": {
            "host": [
                [
                    0,
                    "proc-pb-test-20200425-00000-test-receive-00-0."
                    "receive.sdp",
                ]
            ],
            "port": [[0, 9000]],
            "function": "vlbi",
            "vlbi_beam_id": 1,
        },
    }
}

# These are the receive addresses generated when
# the configuration_string.json file is used for scan_types
# and inconsistent_beam is set to True for create_eb_pb()
RECV_ADDR_DUPLICATE_BEAM = {
    "target:a": RECV_ADDR_FROM_CONFIG_STRING["target:a"],
    "target:b": RECV_ADDR_FROM_CONFIG_STRING["target:a"],
}


def _nested_parameters(pb, parameters: dict) -> dict:
    """
    Convert flattened dictionary to nested dictionary.
    Used in processing parameters.

    :param parameters: Parameters to be converted.
    :type parameters: dict
    :returns: Nested parameters.
    :rtype: dict
    """

    result = {}
    for keys, values in parameters.items():
        _split_rec(pb, keys, values, result)
    return result


def _split_rec(pb, keys: str, values, out: dict) -> None:
    """Splitting keys in dictionary using recursive approach.

    :param keys: Keys from the dictionary.
    :param values: Values from the dictionary.
    :param out: Output result.
    """
    keys, *rest = keys.split(".", 1)
    if rest:
        _split_rec(pb, rest[0], values, out.setdefault(keys, {}))
    else:
        out[keys] = values


@patch.dict(os.environ, MOCK_ENV_VARS)
def test_receive_addresses_set_correctly(pb):
    """
    Receive addresses are set correctly using information
    from the configuration string.
    """
    create_pb_states(pb._config)

    pb_parameters = pb.get_parameters()
    port_start = pb_parameters.get("port_start", 9000)
    scan_types = pb.get_scan_types()
    channels_per_port = pb_parameters.get("channels_per_port", 4)
    recv_address_options = {"num_hosts": 1}

    calculated_recv_addresses, _ = pb.config_host_port_channel_map(
        scan_types, port_start, channels_per_port, **recv_address_options
    )

    # this updates the state of the processing block
    # with the receive addresses
    pb.receive_addresses(
        calculated_recv_addresses,
        chart_name="proc-pb-test-20200425-00000-test-receive",
    )

    for txn in pb._config.txn():
        # get receive addresses back from the pb state
        pb_state = txn.processing_block.state(pb._pb_id).get()
        recv_address = pb_state.get("receive_addresses")
        assert recv_address == RECV_ADDR_FROM_CONFIG_STRING


@patch.dict(os.environ, MOCK_ENV_VARS)
def test_receive_addresses_multiple_scan_type_targets(config_db_client):
    """
    Bugfix:
    When multiple scan_types are present in the configuration string,
    not counting the one with `scan_type_id = .default`,
    the code duplicated the host name in the host string

    The bug was in pb.configure_recv_processes_ports, which produced
    default host-port values for beams that were not part of the
    processed scan_type, but it might have been part of a previous scan_type.

    Note: this behaviour will likely change one
    https://jira.skatelescope.org/browse/ORC-1627 is addressed.
    The current bugfix assumes that the basic implementation is correct
    (which is challenged by ORC-1626)
    """
    with patch(
        "ska_sdp_scripting.processing_block.new_config_client"
    ) as mock_client:
        mock_client.return_value = config_db_client
        create_eb_pb(config_db_client, inconsistent_beam=True)
        create_pb_states(config_db_client)

        pb_id = "pb-test-20200425-00000"
        pb = ProcessingBlock(pb_id)

    pb_parameters = pb.get_parameters()
    port_start = pb_parameters.get("port_start", 9000)
    scan_types = pb.get_scan_types()
    channels_per_port = pb_parameters.get("channels_per_port", 4)
    recv_address_options = {"num_hosts": 1}

    calculated_recv_addresses, _ = pb.config_host_port_channel_map(
        scan_types, port_start, channels_per_port, **recv_address_options
    )

    pb.receive_addresses(
        calculated_recv_addresses,
        chart_name="proc-pb-test-20200425-00000-test-receive",
    )

    for txn in config_db_client.txn():
        # get receive addresses back from the pb state
        pb_state = txn.processing_block.state(pb_id).get()
        recv_address = pb_state.get("receive_addresses")

        assert recv_address == RECV_ADDR_DUPLICATE_BEAM


@patch.dict(os.environ, MOCK_ENV_VARS)
def test_receive_addresses_multiple_addresses(work_phase, pb):
    """
    Test generating and updating receive addresses,
    for multiple channels.

    Scenario: two channels with maximum channels
              per receiver set to 10
    """
    with work_phase:
        for txn in work_phase._config.txn():
            eb_list = txn.execution_block.list_keys()

        for _ in eb_list:
            deploy = work_phase.ee_deploy_helm(
                "test-receive", _nested_parameters(pb, VALUES.copy())
            )

            wait_for_deployments(work_phase, deploy)

            pb_parameters = pb.get_parameters()
            port_start = pb_parameters.get("port_start", 9000)
            channels_per_port = pb_parameters.get("channels_per_port", 10)
            recv_address_options = {"num_hosts": 2}
            (
                calculated_recv_addresses,
                _,
            ) = pb.config_host_port_channel_map(
                SCAN_TYPES,
                port_start,
                channels_per_port,
                **recv_address_options
            )

            pb.receive_addresses(calculated_recv_addresses)

            for txn in work_phase._config.txn():
                pb_state = txn.processing_block.state(PB_ID).get()
                recv_address = pb_state.get("receive_addresses")
                assert recv_address == RECV_ADDRESS


@patch.dict(os.environ, MOCK_ENV_VARS)
def test_receive_addresses_pointing_fqdn(pb):
    """
    Test that receive addresses set without host/port information,
    i.e. when the script setting it is NOT vis-receive.
    """
    create_pb_states(pb._config)

    scan_types = pb.get_scan_types()
    recv_address = {
        scan_type["scan_type_id"]: {
            beam_id: {
                "pointing_cal": "tango://test-sdp/queue_connector/"
                "01/pointing_offsets"
            }
            for beam_id in scan_type["beams"]
        }
        for scan_type in scan_types
    }

    pb.receive_addresses(configured_host_port=recv_address, update_dns=False)

    expected_rec_addrs = RECV_ADDR_FROM_CONFIG_STRING.copy()
    # for pointing, we do not generate hosts and ports
    for value in expected_rec_addrs["target:a"].values():
        value.pop("host")
        value.pop("port")
        value["pointing_cal"] = (
            "tango://test-sdp/queue_connector/01/pointing_offsets"
        )

    for txn in pb._config.txn():
        # get receive addresses back from the pb state
        pb_state = txn.processing_block.state(PB_ID).get()
        recv_address = pb_state.get("receive_addresses")

        assert recv_address == expected_rec_addrs


@patch.dict(os.environ, MOCK_ENV_VARS)
# pylint: disable-next=too-many-locals
def test_dns_name(work_phase, pb):
    """Test generating dns name."""
    pb_parameters = pb.get_parameters()
    port_start = pb_parameters.get("port_start", 9000)
    channels_per_port = pb_parameters.get("channels_per_port", 10)
    recv_address_options = {"num_hosts": 2}
    (
        calculated_recv_addresses,
        _,
    ) = pb.config_host_port_channel_map(
        SCAN_TYPES, port_start, channels_per_port, **recv_address_options
    )

    expected_dns_name = [
        "test-recv-00-0.receive.test-sdp",
        "proc-pb-test-20200425-00000-test-receive-00-0.receive.sdp",
    ]

    with work_phase:
        for txn in work_phase._config.txn():
            eb_list = txn.execution_block.list_keys()

        for _ in eb_list:
            ee_receive = work_phase.ee_deploy_helm("test-receive")

            wait_for_deployments(work_phase, ee_receive)

            # Testing with just passing scan types
            pb.receive_addresses(
                calculated_recv_addresses,
                chart_name=ee_receive.deploy_id,
            )
            for txn in work_phase._config.txn():
                state = txn.processing_block.state(PB_ID).get()
                pb_recv_addresses = state.get("receive_addresses")
                pb_science_host = pb_recv_addresses["target:a"]["vis0"].get(
                    "host"
                )
                assert pb_science_host[0][1] == expected_dns_name[1]

            (
                calculated_recv_addresses1,
                _,
            ) = pb.config_host_port_channel_map(
                SCAN_TYPES,
                port_start,
                channels_per_port,
                **recv_address_options
            )

            pb.receive_addresses(
                calculated_recv_addresses1, "test-recv", "receive", "test-sdp"
            )
            for txn in work_phase._config.txn():
                state = txn.processing_block.state(PB_ID).get()
                pb_receive_addresses = state.get("receive_addresses")
                pb_cal_host = pb_receive_addresses["target:b"]["vis0"].get(
                    "host"
                )
                assert pb_cal_host[0][1] == expected_dns_name[0]


@patch.dict(os.environ, MOCK_ENV_VARS)
# pylint: disable-next=too-many-locals
def test_port(work_phase, pb):
    """Test generating and updating receive addresses."""
    pb_parameters = pb.get_parameters()
    port_start = pb_parameters.get("port_start", 41000)
    channels_per_port = pb_parameters.get("channels_per_port", 2)
    recv_address_options = {"num_hosts": 2}
    (
        calculated_recv_addresses,
        host_port_count,
    ) = pb.config_host_port_channel_map(
        SCAN_TYPES, port_start, channels_per_port, **recv_address_options
    )
    num_nodes = max(
        len(count)
        for beams in host_port_count.values()
        for count in beams.values()
    )

    new_values = VALUES.copy()
    new_values["num_process"] = num_nodes
    assert new_values["num_process"] == 2

    # Get the expected receive addresses from the data file
    receive_addresses_expected = read_json_data(
        "receive_addresses_multiple_ports.json", decode=True
    )

    with work_phase:
        for txn in work_phase._config.txn():
            eb_list = txn.execution_block.list_keys()

        for _ in eb_list:
            ee_receive = work_phase.ee_deploy_helm(
                "test-receive", _nested_parameters(pb, new_values)
            )
            wait_for_deployments(work_phase, ee_receive)

            pb.receive_addresses(calculated_recv_addresses)
            for txn in work_phase._config.txn():
                state = txn.processing_block.state(PB_ID).get()
                pb_receive_addresses = state.get("receive_addresses")
                assert pb_receive_addresses == receive_addresses_expected


def test_configure_recv_processes_ports(pb):
    """
    Test that a correct dictionary is configured for receive addresses,
    which is updated with port and function and provides an initial
    list of hosts.
    """
    expected_recv_dict = {
        "target:a": {
            "vis0": {
                "host": [(0, 0), (2004, 1)],
                "port": [(0, 9000), (2000, 9001), (2004, 9000)],
            }
        },
        "target:b": {
            "vis0": {
                "host": [(0, 0), (2000, 1)],
                "port": [(0, 9000), (2000, 9000)],
            }
        },
    }

    pb_parameters = pb.get_parameters()
    port_start = pb_parameters.get("port_start", 9000)
    channels_per_port = pb_parameters.get("channels_per_port", 4)
    recv_address_options = {"num_hosts": 2}
    (
        result_recv_dict,
        result_host_port_count,
    ) = pb.config_host_port_channel_map(
        SCAN_TYPES, port_start, channels_per_port, **recv_address_options
    )
    num_nodes = max(
        len(count)
        for beams in result_host_port_count.values()
        for count in beams.values()
    )

    assert num_nodes == 2
    assert result_recv_dict == expected_recv_dict


def test_add_config_beam_info_to_recv_addrs(pb):
    """
    Test that "function" is added correctly to beams in
    receive addresses.

    It also adds {x}_beam_id to receive_address if present
    in the configuration string,
    which in the test case is present for the pss1 beam.
    The key for this is "search_beam_id".

    Depending on beam type, we get the correct
    "function" and "{x}_beam_key" added.
    """
    receive_addresses = deepcopy(RECV_ADDR_FROM_CONFIG_STRING)
    del receive_addresses["target:a"]["vis0"]["function"]
    del receive_addresses["target:a"]["pss1"]["function"]
    del receive_addresses["target:a"]["pss1"]["search_beam_id"]
    assert receive_addresses != RECV_ADDR_FROM_CONFIG_STRING

    pb._add_config_beam_info_to_recv_addrs(receive_addresses)

    assert receive_addresses == RECV_ADDR_FROM_CONFIG_STRING


def test_get_scan_types(pb):
    """
    get_scan_types resolves scan_type, beam, and channel information
    correctly into a combined scan_types list of dicts.
    """
    create_pb_states(pb._config)

    scan_types = pb.get_scan_types()
    assert len(scan_types) == len(EXPECTED_SCAN_TYPES) == 1
    assert scan_types[0].keys() == EXPECTED_SCAN_TYPES[0].keys()
    assert (
        scan_types[0]["beams"].keys() == EXPECTED_SCAN_TYPES[0]["beams"].keys()
    )
    for k, v in scan_types[0]["beams"].items():
        assert v == EXPECTED_SCAN_TYPES[0]["beams"][k]


@pytest.mark.parametrize(
    (
        "count",
        "start",
        "stride",
        "channels_per_port",
        "num_hosts",
        "hosts",
        "ports",
        "port_count",
    ),
    (
        (1, 0, 1, 1, 1, [(0, 0)], [(0, 10000, 1)], [1]),
        (5, 0, 1, 1, 1, [(0, 0)], [(0, 10000, 1)], [5]),
        (
            4,
            0,
            3,
            1,
            1,
            [(0, 0)],
            [(0, 10000), (3, 10001), (6, 10002), (9, 10003)],
            [4],
        ),
        (5, 500, 1, 1, 1, [(500, 0)], [(500, 10000, 1)], [5]),
        (
            60,
            0,
            1,
            20,
            1,
            [(0, 0)],
            [(0, 10000), (20, 10001), (40, 10002)],
            [3],
        ),
        (
            60,
            0,
            3,
            20,
            1,
            [(0, 0)],
            [(0, 10000), (60, 10001), (120, 10002)],
            [3],
        ),
        (10, 0, 1, 5, 1, [(0, 0)], [(0, 10000), (5, 10001)], [2]),
        (10, 400, 1, 5, 1, [(400, 0)], [(400, 10000), (405, 10001)], [2]),
        (10, 0, 2, 5, 1, [(0, 0)], [(0, 10000), (10, 10001)], [2]),
        (
            20,
            0,
            2,
            5,
            2,
            [(0, 0), (20, 1)],
            [(0, 10000), (10, 10001), (20, 10000), (30, 10001)],
            [2, 2],
        ),
    ),
)
# pylint: disable-next=too-many-arguments,too-many-positional-arguments
def test_single_sw_addresses_fixed_num_hosts(
    count,
    start,
    stride,
    channels_per_port,
    num_hosts,
    hosts,
    ports,
    port_count,
):
    """Test single spectral windows for fixed number of hosts"""
    spectral_windows = [{"count": count, "start": start, "stride": stride}]
    (
        result_hosts,
        result_ports,
        result_port_count,
    ) = generate_host_port_per_beam(
        spectral_windows, 10000, channels_per_port, num_hosts=num_hosts
    )
    assert len(result_hosts) == len(result_port_count)
    assert result_hosts == hosts
    assert result_ports == ports
    assert result_port_count == port_count


@pytest.mark.parametrize(
    (
        "count",
        "start",
        "stride",
    ),
    (
        (1, 0, 1),
        (5, 0, None),
        (5, 0, "NOT_GIVEN"),
    ),
)
def test_sw_stride(count, start, stride):
    """
    Bugfix: SKB-588.  Check that stride parameter defaults to 1.
    """
    if stride == "NOT_GIVEN":
        spectral_windows = [{"count": count, "start": start}]
    else:
        spectral_windows = [{"count": count, "start": start, "stride": stride}]

    (
        result_hosts,
        result_ports,
        result_port_count,
    ) = generate_host_port_per_beam(spectral_windows, 10000, 1, num_hosts=1)

    assert len(result_hosts) == len(result_port_count)
    assert result_ports == [(0, 10000, 1)]
    assert result_port_count == [count]
