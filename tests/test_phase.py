# pylint: disable=protected-access

"""
Phase tests
"""

import logging
from unittest.mock import patch

import pytest
from pydantic_core import Url
from ska_sdp_config.entity import Flow
from ska_sdp_config.entity.flow import DataQueue

from ska_sdp_scripting.phase import Phase
from ska_sdp_scripting.utils import DeploymentStatus
from tests.utils import PB_ID, create_pb_states, create_work_phase

LOG = logging.getLogger("ska_sdp_scripting")


# pylint: disable-next=too-few-public-methods
class DataFlow:
    """Wrapper for data flows"""

    def __init__(self, flow):
        self.flow = flow


@pytest.fixture(name="data_flow")
def data_flow_fxt():
    """Data Flow fixture"""
    flow_key = {"pb_id": PB_ID, "kind": "data-product", "name": "flow1"}
    flow_sink = {
        "kind": "data-product",
        "data_dir": "some/output/directory",
        "paths": ["some/path/file1", "some/path/file2"],
    }

    flow = Flow(
        key=Flow.Key(**flow_key),
        sink=flow_sink,
        sources=[],
        data_model="Model1",
    )
    return flow


def _create_deployment_states(txn, deploy_ids, deploy_state):
    """
    Create deployment states for testing monitoring
    """
    pb_state = txn.processing_block.state(PB_ID).get()
    assert "deployments_ready" not in pb_state.keys()

    # create state for deployments
    # code will update aggregate state based on this information
    deployment_list = txn.deployment.list_keys()
    for i, dpl_id in enumerate(deploy_ids):
        assert (
            pb_state["deployments"][dpl_id] == DeploymentStatus.PENDING.value
        )
        assert dpl_id in deployment_list
        txn.deployment.state(dpl_id).create(deploy_state[i])
        dpl_state = txn.deployment.state(dpl_id).get()
        assert dpl_state == deploy_state[i]


def test_monitor_deployments_all_running(work_phase):
    """
    Test that Phase.monitor_deployments updates individual
    and aggregate state of deployments (in the form of the
    deployments_ready key) within the processing block state
    correctly when all deployment pods are RUNNING.
    """
    deploy_names = ["cbf-sdp-emulator", "receiver"]
    deploy_ids = [
        f"proc-{PB_ID}-{deploy_name}" for deploy_name in deploy_names
    ]
    deploy_state = [
        {
            "num_pod": 1,
            "pods": {
                f"{deploy_ids[0]}-abcdz1": {
                    "status": DeploymentStatus.RUNNING.value
                }
            },
        },
        {
            "num_pod": 1,
            "pods": {
                f"{deploy_ids[1]}-safda1": {
                    "status": DeploymentStatus.RUNNING.value
                }
            },
        },
    ]

    with work_phase:
        for name in deploy_names:
            work_phase.ee_deploy_helm(name)

        for txn in work_phase._config.txn():
            _create_deployment_states(txn, deploy_ids, deploy_state)

            work_phase.monitor_deployments(txn)

            pb_state = txn.processing_block.state(PB_ID).get()
            for _, dpl_id in enumerate(deploy_ids):
                assert (
                    pb_state["deployments"][dpl_id]
                    == DeploymentStatus.RUNNING.value
                )

                assert pb_state["deployments_ready"] is True
                assert pb_state["error_messages"] == {}
                assert pb_state["status"] == "RUNNING"


def test_monitor_deployments_some_pending(work_phase):
    """
    Test that Phase.monitor_deployments updates individual
    and aggregate state of deployments (in the form of the
    deployments_ready key) within the processing block state
    correctly when some deployment pods are PENDING.
    """
    deploy_names = ["cbf-sdp-emulator", "receiver"]
    deploy_ids = [
        f"proc-{PB_ID}-{deploy_name}" for deploy_name in deploy_names
    ]
    deploy_state = [
        {
            "num_pod": 1,
            "pods": {
                f"{deploy_ids[0]}-abcdz1": {
                    "status": DeploymentStatus.PENDING.value
                }
            },
        },
        {
            "num_pod": 1,
            "pods": {
                f"{deploy_ids[1]}-safda1": {
                    "status": DeploymentStatus.RUNNING.value
                }
            },
        },
    ]

    with work_phase:
        for name in deploy_names:
            work_phase.ee_deploy_helm(name)

        for txn in work_phase._config.txn():
            _create_deployment_states(txn, deploy_ids, deploy_state)

            work_phase.monitor_deployments(txn)

            pb_state = txn.processing_block.state(PB_ID).get()
            for i, dpl_id in enumerate(deploy_ids):
                assert (
                    pb_state["deployments"][dpl_id]
                    == list(deploy_state[i]["pods"].values())[0]["status"]
                )
                assert pb_state["deployments_ready"] is False
                assert pb_state["error_messages"] == {}
                assert pb_state["status"] == "RUNNING"


def test_monitor_deployment_recovers(work_phase):
    """
    Test that error messages are correctly applied to the
    pb state and correctly removed when a deployment
    recovers.
    """
    deploy_name = "cbf-sdp-emulator"
    deploy_id = f"proc-{PB_ID}-{deploy_name}"

    failed_state = {
        "num_pod": 2,
        "pods": {
            f"{deploy_id}-abcdz1": {
                "error_state": "ImgPullErr",
                "status": DeploymentStatus.FAILED.value,
            },
            f"{deploy_id}-abcdz2": {"status": DeploymentStatus.RUNNING.value},
        },
    }

    recovered_state = {
        "num_pod": 2,
        "pods": {
            f"{deploy_id}-abcdz1": {"status": DeploymentStatus.RUNNING.value},
            f"{deploy_id}-abcdz2": {"status": DeploymentStatus.RUNNING.value},
        },
    }

    with work_phase:
        work_phase.ee_deploy_helm(deploy_name)

        for txn in work_phase._config.txn():
            _create_deployment_states(txn, [deploy_id], [failed_state])

            work_phase.monitor_deployments(txn)

            # assert error messages have been captured correctly
            pb_state = txn.processing_block.state(PB_ID).get()
            assert pb_state["error_messages"] == {deploy_id: ["ImgPullErr"]}
            assert pb_state["status"] == "FAILED"

            # update deployment state (simulating helm deployer)
            txn.deployment.state(deploy_id).update(recovered_state)
            dpl_state = txn.deployment.state(deploy_id).get()
            assert dpl_state == recovered_state

            # assert that the error messages have been removed
            work_phase.monitor_deployments(txn)

            pb_state = txn.processing_block.state(PB_ID).get()
            assert (
                pb_state["error_messages"].get(deploy_id) is None
                or deploy_id not in pb_state["error_messages"]
            )


def test_monitor_deployments_some_failed(work_phase):
    """
    Test that Phase.monitor_deployments updates individual
    and aggregate state of deployments (in the form of the
    deployments_ready key) within the processing block state
    correctly when some deployment pods are FAILED.
    """
    deploy_names = ["cbf-sdp-emulator", "receiver", "receiver-2"]
    deploy_ids = [
        f"proc-{PB_ID}-{deploy_name}" for deploy_name in deploy_names
    ]
    deploy_state = [
        {
            "num_pod": 2,
            "pods": {
                f"{deploy_ids[0]}-abcdz1": {
                    "error_state": "ImgPullErr",
                    "status": DeploymentStatus.FAILED.value,
                },
                f"{deploy_ids[0]}-abcdz2": {
                    "status": DeploymentStatus.RUNNING.value
                },
            },
        },
        {
            "num_pod": 2,
            "pods": {
                f"{deploy_ids[0]}-abedz1": {
                    "status": DeploymentStatus.RUNNING.value
                },
                f"{deploy_ids[1]}-abedz2": {
                    "error_state": "ValueErr",
                    "status": DeploymentStatus.FAILED.value,
                },
            },
        },
        {
            "num_pod": 1,
            "pods": {
                f"{deploy_ids[2]}-safda1": {
                    "status": DeploymentStatus.RUNNING.value
                }
            },
        },
    ]

    with work_phase:
        for name in deploy_names:
            work_phase.ee_deploy_helm(name)

        for txn in work_phase._config.txn():
            _create_deployment_states(txn, deploy_ids, deploy_state)

            work_phase.monitor_deployments(txn)

            pb_state = txn.processing_block.state(PB_ID).get()
            assert pb_state["deployments"][deploy_ids[0]] == "FAILED"

            assert pb_state["error_messages"] == {
                "proc-pb-test-20200425-00000-cbf-sdp-emulator": ["ImgPullErr"],
                "proc-pb-test-20200425-00000-receiver": ["ValueErr"],
            }
            assert pb_state["status"] == "FAILED"


def test_monitor_deployments_missing_pods(work_phase):
    """
    deployments_ready = False, because the deploy_state indicates
    that we will have 2 pods running, but only one of them started
    and is in READY start. Value would only switch to True, if
    both pods' information is in the deploy_state with RUNNING status.
    """
    deploy_id = f"proc-{PB_ID}-cbf-sdp-emulator"
    deploy_state = {
        "num_pod": 2,
        "pods": {
            f"{deploy_id}-safda1": {"status": DeploymentStatus.RUNNING.value}
        },
    }

    with work_phase:
        work_phase.ee_deploy_helm("cbf-sdp-emulator")

        for txn in work_phase._config.txn():
            _create_deployment_states(txn, [deploy_id], [deploy_state])

            work_phase.monitor_deployments(txn)

            pb_state = txn.processing_block.state(PB_ID).get()

            assert (
                pb_state["deployments"][deploy_id]
                == DeploymentStatus.PENDING.value
            )
            assert pb_state["deployments_ready"] is False


def test_monitor_deployments_none(work_phase):
    """
    When deployments are assigned to a PB but they don't exist,
    their state in PB state is set to PENDING and the aggregate
    "deployments_ready" key is False.
    """
    deploy_names = ["cbf-sdp-emulator", "receiver"]
    deploy_ids = [
        f"proc-{PB_ID}-{deploy_name}" for deploy_name in deploy_names
    ]

    with work_phase:
        for name in deploy_names:
            work_phase.ee_deploy_helm(name)

        for txn in work_phase._config.txn():
            for dpl_id in deploy_ids:
                dpl_state = txn.deployment.state(dpl_id).get()
                assert dpl_state is None

            work_phase.monitor_deployments(txn)

            pb_state = txn.processing_block.state(PB_ID).get()
            for dpl_id in deploy_ids:
                assert (
                    pb_state["deployments"][dpl_id]
                    == DeploymentStatus.PENDING.value
                )
                assert pb_state["deployments_ready"] is False


def test_monitor_deployments_none_pods(work_phase):
    """
    When deployments are assigned to a PB and they exist,
    but they don't have any pods, then
    their state in PB state is set to PENDING and the aggregate
    "deployments_ready" key is False.
    """
    deploy_name = "cbf-sdp-emulator"
    deploy_id = f"proc-{PB_ID}-{deploy_name}"

    with work_phase:
        work_phase.ee_deploy_helm(deploy_name)

        for txn in work_phase._config.txn():
            txn.deployment.state(deploy_id).create({})
            dpl_state = txn.deployment.state(deploy_id).get()
            assert "pods" not in dpl_state

            work_phase.monitor_deployments(txn)

            pb_state = txn.processing_block.state(PB_ID).get()
            assert (
                pb_state["deployments"][deploy_id]
                == DeploymentStatus.PENDING.value
            )

            assert pb_state["deployments_ready"] is False


def test_monitor_deployments_no_depls(work_phase):
    """
    When a processing block doesn't deploy anything
    the "deployments" entry in the PB state is {}
    and "deployments_ready" is set to True
    """
    with work_phase:
        for txn in work_phase._config.txn():
            pb_state = txn.processing_block.state(PB_ID).get()
            assert pb_state["deployments"] == {}

            work_phase.monitor_deployments(txn)

            pb_state = txn.processing_block.state(PB_ID).get()
            assert pb_state["deployments"] == {}
            assert pb_state["deployments_ready"] is True


def test_phase_wait_loop(work_phase):
    """
    Test that wait_loop waits for condition to be True
    before exiting. It also tests that monitor_deployment
    is running as part of wait_loop.

    This test only makes sense if a real etcd DB is used.
    (for manual run, start an etcd db and set
    FEATURE_CONFIG_DB=1)
    """
    deploy_names = ["cbf-sdp-emulator", "receiver"]
    deploy_ids = [
        f"proc-{PB_ID}-{deploy_name}" for deploy_name in deploy_names
    ]
    deploy_state = [
        {
            "num_pod": 1,
            "pods": {
                f"{deploy_ids[0]}-abcdz1": {
                    "status": DeploymentStatus.RUNNING.value
                }
            },
        },
        {
            "num_pod": 1,
            "pods": {
                f"{deploy_ids[1]}-safda1": {
                    "status": DeploymentStatus.RUNNING.value
                }
            },
        },
    ]

    class WaitAndGo:
        # pylint: disable=too-few-public-methods
        """
        This class imitates something that changes continuously,
        e.g. like the state of an execution block.
        The watcher needs to wait for this to return True,
        but the watcher loop only moves to its next iteration if
        something in the db changes, hence why I'm updating the
        PB state with something random, to trigger a new iteration
        and hence a new call to this class.

        We are counting the number of times this was called.
        When the count reaches 10, we'll return True
        and hence the watcher loop can exit.
        """

        def __init__(self):
            self.called = 0

        def __call__(self, transaction, *args, **kwargs):
            self.called += 1

            pb_ste = transaction.processing_block.state(PB_ID).get()
            pb_ste["tmp"] = self.called
            transaction.processing_block.state(PB_ID).update(pb_ste)

            if self.called == 10:
                return True
            return False

    with work_phase:
        for name in deploy_names:
            work_phase.ee_deploy_helm(name)

        for txn in work_phase._config.txn():
            _create_deployment_states(txn, deploy_ids, deploy_state)

        # monitor_deployments also runs as part of wait_loop
        work_phase.wait_loop(WaitAndGo())

    for txn in work_phase._config.txn():
        pb_state = txn.processing_block.state(PB_ID).get()
        for i, dpl_id in enumerate(deploy_ids):
            assert (
                pb_state["deployments"][dpl_id]
                == list(deploy_state[i]["pods"].values())[0]["status"]
            )
            assert pb_state["deployments_ready"] is True


@pytest.mark.parametrize("dpl_status", ["FINISHED", "CANCELLED"])
def test_phase_dpl_clean_up(dpl_status, work_phase):
    """
    Phase correctly cleans up deployments in __exit__,
    if deployment is FINISHED or CANCELLED
    """
    deploy_name = "receiver"
    deploy_id = f"proc-{PB_ID}-{deploy_name}"

    with work_phase:
        for txn in work_phase._config.txn():
            # no deployments are created yet
            deployment_list = txn.deployment.list_keys()
            assert len(deployment_list) == 0

        work_phase.ee_deploy_helm(deploy_name)

        for txn in work_phase._config.txn():
            # ee_deploy_helm created the deployment
            # its status at this point is PENDING
            deployment_list = txn.deployment.list_keys()
            assert len(deployment_list) == 1
            assert deployment_list[0] == deploy_id

            # manually set deployment status to FINISHED
            pb_state = txn.processing_block.state(PB_ID).get()
            pb_state["deployments"][deploy_id] = dpl_status
            txn.processing_block.state(PB_ID).update(pb_state)

        # we still have deployment in the Config DB
        for txn in work_phase._config.txn():
            deployment_list = txn.deployment.list_keys()
            assert len(deployment_list) == 1

    # once the phase exits, the deployment is removed
    for txn in work_phase._config.txn():
        deployment_list = txn.deployment.list_keys()
        assert len(deployment_list) == 0


def test_phase_flow_clean_up(pb):
    """
    Phase correctly cleans up flows in __exit__
    """
    create_pb_states(pb._config)

    queue = DataQueue(
        topics="test-topic",
        host=Url("kafka://some-host"),
        format="msgpack_numpy",
    )

    phase = create_work_phase(
        pb,
        flows=[
            {
                "name": "test-flow",
                "data_model": "PointingTable",
                "sink": queue,
            }
        ],
    )

    # set resources_available = True for phase.__enter__ to finish
    for txn in phase._config.txn():
        pb_state = txn.processing_block.state(PB_ID).get()
        pb_state["resources_available"] = True
        txn.processing_block.state(PB_ID).update(pb_state)

    with phase:
        for txn in phase._config.txn():
            # no deployments are created yet
            flows = txn.flow.list_keys()
            assert len(flows) == 1
            result_flow_key = flows[0]
            txn.flow.ownership(result_flow_key).take()

            flow_state = txn.flow.state(result_flow_key).get()
            flow_owner = txn.flow.ownership(result_flow_key).get()
            assert flow_state is not None
            assert flow_owner is not None

    # once the phase exits, the phase, its state and owner are removed
    for txn in phase._config.txn():
        flows = txn.flow.list_keys()
        assert len(flows) == 0

        flow_state = txn.flow.state(result_flow_key).get()
        flow_owner = txn.flow.ownership(result_flow_key).get()
        assert flow_state is None
        assert flow_owner is None


def test_monitor_data_flows(work_phase, data_flow):
    """
    Test that monitor_data_flows correctly monitors data flows
    """

    work_phase.flows = [DataFlow(data_flow)]
    with work_phase:
        for txn in work_phase._config.txn():
            work_phase.monitor_data_flows(txn)

        # Check that error_messages in pb_state is empty
        for txn in work_phase._config.txn():
            pb_state = txn.processing_block.state(PB_ID).get()
            error_messages = pb_state.get("error_messages", {})
            data_flow_key = str(data_flow.key)
            assert data_flow_key not in error_messages


def test_monitor_data_flows_error_and_resolved(work_phase, data_flow):
    """
    Test that monitor_data_flows adds errors and removes
    them from pb_state when resolved.
    """
    work_phase.flows = [DataFlow(data_flow)]

    with work_phase:
        for txn in work_phase._config.txn():
            data_flow_key = data_flow.key
            flow_state = {
                "status": "pending",
                "log": f"Error message for {data_flow_key.kind}",
            }
            txn.flow.state(data_flow_key).update(flow_state)

            work_phase.monitor_data_flows(txn)

            # Check that error_messages is updated
            pb_state = txn.processing_block.state(PB_ID).get()
            error_messages = pb_state.get("error_messages")
            data_flow_key = data_flow.key
            kind = data_flow_key.kind
            assert kind in error_messages
            error_entry = error_messages[kind]
            assert error_entry["key"] == data_flow_key.dict()
            assert error_entry["error_message"] == f"Error message for {kind}"
            assert pb_state["status"] == "RUNNING"

            # Remove the error from the data flow state
            data_flow_key = data_flow.key
            flow_state = txn.flow.state(data_flow_key).get()
            if "log" in flow_state:
                del flow_state["log"]
            txn.flow.state(data_flow_key).update(flow_state)

            # call monitor again and confirm the error is gone
            work_phase.monitor_data_flows(txn)

            pb_state = txn.processing_block.state(PB_ID).get()
            error_messages = pb_state.get("error_messages")

            kind = data_flow.key.kind
            assert kind not in error_messages


def test_get_deployment_status_slurm(config_db_client):
    """
    Test that _get_deployment_status correctly processes
    Slurm deployment states.
    """
    for txn in config_db_client.txn():
        slurm_deployment_state = {
            "num_job": 5,
            "jobs": {
                "job_1": {"status": DeploymentStatus.FAILED.value},
                "job_2": {"status": DeploymentStatus.RUNNING.value},
            },
            "status": DeploymentStatus.FAILED.value,
        }
        with patch(
            "ska_sdp_scripting.phase.Phase._get_deployment_status",
            return_value=slurm_deployment_state,
        ):
            deployment_state = Phase._get_deployment_status(
                txn, "deploy_slurm"
            )
            assert "num_job" in deployment_state
            assert "jobs" in deployment_state
            assert deployment_state["status"] == DeploymentStatus.FAILED.value


def test_get_deployment_status_kubernetes(config_db_client):
    """
    Test that _get_deployment_status correctly processes
    Kubernetes deployment states.
    """
    for txn in config_db_client.txn():
        k8s_deployment_state = {
            "num_pod": 3,
            "pods": {
                "pod_1": {"status": DeploymentStatus.CANCELLED.value},
                "pod_2": {"status": DeploymentStatus.RUNNING.value},
            },
            "status": DeploymentStatus.CANCELLED.value,
        }
        with patch(
            "ska_sdp_scripting.phase.Phase._get_deployment_status",
            return_value=k8s_deployment_state,
        ):
            deployment_state = Phase._get_deployment_status(
                txn, "deploy_kubernetes"
            )
            assert "num_pod" in deployment_state
            assert "pods" in deployment_state
            assert (
                deployment_state["status"] == DeploymentStatus.CANCELLED.value
            )
