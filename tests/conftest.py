# pylint: disable=protected-access

"""Pytest fixtures."""

from unittest.mock import patch

import pytest

from ska_sdp_scripting import ProcessingBlock, config, new_config_client
from tests.utils import (
    PB_ID,
    create_eb_pb,
    create_pb_states,
    create_work_phase,
)

# Use the config DB memory backend. This will be overridden if the
# FEATURE_CONFIG_DB environment variable is set to 1.
config.FEATURE_CONFIG_DB.set_default(False)


@pytest.fixture(scope="session", name="db_prefix")
def db_prefix_fxt():
    """Fixture for database entry prefix."""
    return "/__test_scripting"


@pytest.fixture(scope="function", name="config_db_client")
def config_db_client_fxt(db_prefix):
    """Generate a config DB Client for the session"""
    config_client = new_config_client(prefix=db_prefix)
    config_client.backend.delete(db_prefix, must_exist=False, recursive=True)
    yield config_client
    config_client.backend.delete(db_prefix, must_exist=False, recursive=True)
    config_client.close()


@pytest.fixture(name="pb")
def processing_block_fxt(config_db_client):
    """
    Add pbs and an eb to config db and
    create a processing block with id PB_ID
    """
    with patch(
        "ska_sdp_scripting.processing_block.new_config_client"
    ) as mock_client:
        mock_client.return_value = config_db_client
        create_eb_pb(config_db_client)

        return ProcessingBlock(PB_ID)


@pytest.fixture(name="work_phase")
def work_phase_fxt(pb):
    """
    Create a phase for pb (of id PB_ID)
    """
    create_pb_states(pb._config)
    phase = create_work_phase(pb)

    # set resources_available = True for phase.__enter__ to finish
    for txn in pb._config.txn():
        pb_state = txn.processing_block.state(pb._pb_id).get()
        pb_state["resources_available"] = True
        txn.processing_block.state(pb._pb_id).update(pb_state)

    return phase
