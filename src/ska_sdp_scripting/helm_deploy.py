"""Helm Deploy class module for SDP scripting."""

import logging

from ska_sdp_config import Config, Deployment

from .ee_base_deploy import EEDeploy
from .utils import DeploymentStatus

LOG = logging.getLogger("ska_sdp_scripting")


class HelmDeploy(EEDeploy):
    """
    Deploy Helm execution engine.

    This should not be created directly, use the :func:`Phase.ee_deploy_helm`
    method instead.

    :param pb_id: processing block ID
    :type pb_id: str
    :param config: SDP configuration client
    :type config: ska_sdp_config.Config
    :param deploy_name: name of Helm chart to deploy
    :type deploy_name: str
    :param values: values to pass to Helm chart
    :type values: dict, optional
    """

    def __init__(
        self,
        pb_id: str,
        config: Config,
        deploy_name: str,
        values: dict = None,
        version: str = "1.0.0",
    ):
        super().__init__(pb_id, config)
        self._deploy(deploy_name, values, version)

    def _deploy(
        self, deploy_name: str, values: dict = None, version: str = "1.0.0"
    ) -> None:
        """
        Deploy the Helm chart.

        :param deploy_name: deployment name
        :param values: optional dict of values
        :param version: helm chart version, defaults to 1.0.0

        """
        LOG.info("Deploying Helm chart: %s, version %s", deploy_name, version)
        self._deploy_id = f"proc-{self._pb_id}-{deploy_name}"

        # upon creation, the deployment status in processing
        # block state should always start with PENDING
        self.update_deploy_status(DeploymentStatus.PENDING.value)

        args = {
            "chart": deploy_name,  # Helm chart deploy from the repo
            "version": version,
        }

        if values is not None:
            args["values"] = values

        deploy = Deployment(key=self._deploy_id, kind="helm", args=args)
        for txn in self._config.txn():
            txn.deployment.create(deploy)
