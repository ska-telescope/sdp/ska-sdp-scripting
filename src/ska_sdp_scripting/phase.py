"""Phase class module for SDP scripting."""

import logging
from copy import deepcopy
from typing import Any, Callable, Optional

from ska_sdp_config import Config
from ska_sdp_config.config import Transaction

from ska_sdp_scripting.data_flow import DataFlow

from .dask_deploy import DaskDeploy
from .fake_deploy import FakeDeploy
from .helm_deploy import HelmDeploy
from .queue_connector import wait_for_queue_connector_state
from .slurm_deploy import SlurmDeploy
from .utils import (
    DeploymentStatus,
    ExecutionBlockStatus,
    FlowStatus,
    ProcessingBlockStatus,
)

LOG = logging.getLogger("ska_sdp_scripting")
TIMEOUT_SEC = 60.0

# Processing block states
DONE_STATES = {
    ProcessingBlockStatus.FINISHED.value,
    ProcessingBlockStatus.CANCELLED.value,
}


class PhaseException(Exception):
    """Exception raised by a script phase."""


# pylint: disable-next=too-many-instance-attributes
class Phase:
    """
    This class encapsulates the logic for managing a single phase within a
    processing block.

    Objects of this class should not be created directly. use the
    :py:meth:`ProcessingBlock.create_phase
    <.processing_block.ProcessingBlock.create_phase()>` method instead.

    The key functionalities include:

    - **Resource Management:**
        Handles the allocation and release of resources required by the phase,
        including interacting with the queue connector.
    - **Execution Engine Deployment:**
        Provides methods for deploying various types of execution engines
        (e.g., :py:meth:`ee_deploy_helm`, :py:meth:`ee_deploy_dask`,
        :py:meth:`ee_deploy_test`).
    - **State Management:**
        Monitors the state of deployments and data flows, updating the
        processing block state accordingly. Includes methods like
        :py:meth:`check_state`, :py:meth:`update_pb_state`,
        :py:meth:`monitor_deployments`, and :py:meth:`monitor_data_flows`.
    - **Context Management:**
        Designed to be used with the `with` statement (:py:meth:`__enter__` and
        :py:meth:`__exit__` methods) to ensure proper setup and teardown of
        resources.

    :param name: Name of the phase.
    :type name: str
    :param list_requests: List of requests.
    :type list_requests: list
    :param config: SDP configuration client.
    :type config: :class:`ska_sdp_config.Config`
    :param pb_id: Processing block ID.
    :type pb_id: str
    :param eb_id: Execution block ID.
    :type eb_id: str
    :param script_kind: Script kind.
    :type script_kind: str
    :param flow_data: Dataflow list.
    :type flow_data: list[DataFlow]
    """

    # pylint: disable-next=too-many-arguments,too-many-positional-arguments
    def __init__(
        self,
        name: str,
        list_requests: list,
        config: Config,
        pb_id: str,
        eb_id: str,
        script_kind: str,
        flow_data: list[DataFlow],
    ):
        self._name = name
        self._requests = list_requests
        self._config = config
        self._pb_id = pb_id
        self._eb_id = eb_id
        self._script_kind = script_kind
        self.flows = flow_data
        self._deploy_id_list = []
        self._deploy = None
        self._status = None
        self._deployment_status = None

    def __enter__(self):
        """
        Wait for resources to become available.

        While waiting, it checks if the :py:class:`ProcessingBlock
        <.processing_block.ProcessingBlock>` is cancelled or finished, and for
        real-time scripts it checks if the :py:class:`ExecutionBlock
        <ska_sdp_config.entity.eb.ExecutionBlock>` is cancelled or finished.
        """
        # Set state to indicate script is waiting for resources
        for attempt, txn in enumerate(self._config.txn()):
            self.check_state(txn)
            LOG.info("Setting status to WAITING (attempt %s)", attempt + 1)
            state = txn.processing_block.state(self._pb_id).get()
            state["status"] = ProcessingBlockStatus.WAITING.value
            txn.processing_block.state(self._pb_id).update(state)

        wait_for_queue_connector_state(
            self._config, self._pb_id, None, timeout=TIMEOUT_SEC
        )

        LOG.info("Waiting for resources to be available")
        resources_available = False
        for watcher in self._config.watcher():
            for attempt, txn in enumerate(watcher.txn(), 1):
                self.check_state(txn)
                state = txn.processing_block.state(self._pb_id).get()
                resources_available = state.get("resources_available")
                if resources_available:
                    LOG.info("Resources are available (attempt %s)", attempt)
                    self._update_pb_state_upon_start(txn, state, attempt)

            # Finish watcher loop once we have resources available
            if resources_available:
                break

        # commit data flow entries
        for txn in self._config.txn():
            for data_flow in self.flows:
                flow = data_flow.flow
                LOG.debug("Creating flow: %s", flow.key)
                txn.flow.create(flow)
                txn.flow.state(flow.key).create(
                    {"status": FlowStatus.PENDING.value}
                )

        LOG.info("Checking queue connector state")
        wait_for_queue_connector_state(
            self._config,
            self._pb_id,
            FlowStatus.FLOWING.value,
            timeout=TIMEOUT_SEC,
        )

        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        """
        Remove resources created during this phase:
        - queue connector entry
        - deployments
        - flows
        """

        # Clean up deployment.
        LOG.info("Clean up deployments.")
        if self._deploy_id_list:
            for txn in self._config.txn():
                state = txn.processing_block.state(self._pb_id).get()
                deployments = state.get("deployments")
                keys = txn.deployment.list_keys()
                for deploy_id in self._deploy_id_list:
                    if (
                        deployments[deploy_id] in DONE_STATES
                        and deploy_id in keys
                    ):
                        self._deploy.remove(deploy_id)

        # Clean up flows.
        LOG.info("Clean up flows.")
        for flow in self.flows:
            for txn in self._config.txn():
                txn.flow.delete(flow.flow, recurse=True)

        self.update_pb_state()
        LOG.info("Deployments All Done")

    def _update_pb_state_upon_start(
        self, txn: Transaction, state: dict, attempt: int
    ) -> None:
        """
        Update processing block state
        """
        # Add deployments key to processing block state
        if "deployments" not in state:
            state["deployments"] = {}

        # Set state to indicate processing has started
        LOG.info("Setting status to RUNNING (attempt %s)", attempt)
        state["status"] = ProcessingBlockStatus.RUNNING.value

        txn.processing_block.state(self._pb_id).update(state)

    def check_state(
        self, txn: Transaction, check_realtime: bool = True
    ) -> None:
        """
        Check the state of the processing block.

        Check if the :py:class:`ProcessingBlock
        <.processing_block.ProcessingBlock>` state is finished or cancelled,
        and for real-time scripts check if the :py:class:`ExecutionBlock
        <ska_sdp_config.entity.eb.ExecutionBlock>` is finished or cancelled.

        :param txn: SDP configuration transaction.
        :type txn: :py:class:`Transaction <ska_sdp_config.config.Transaction>`
        :param check_realtime: Whether to check execution block state if the
            processing block is realtime (i.e. cancel processing script for
            FINISHED/CANCELLED)
        :type check_realtime: bool

        """
        LOG.info("Checking processing block state")
        pb_state = txn.processing_block.state(self._pb_id).get()
        if pb_state is None:
            raise PhaseException("Processing block was deleted")

        if pb_state.get("status") in DONE_STATES:
            raise PhaseException(f"Processing block is {pb_state}")

        if not txn.processing_block.ownership(
            self._pb_id
        ).is_owned_by_this_process():
            raise PhaseException("Lost ownership of the processing block")

        if self._script_kind == "realtime" and check_realtime:
            LOG.info("Checking execution block state")
            eb_state = txn.execution_block.state(self._eb_id).get()
            if eb_state is None:
                raise PhaseException("ExecutionBlock state not found.")

            if (eb_status := eb_state.get("status")) in DONE_STATES:
                raise PhaseException(f"ExecutionBlock is {eb_status}")

    def ee_deploy_test(
        self,
        deploy_name: str,
        func: Callable = None,
        f_args: list[Any] = None,
    ) -> FakeDeploy:
        """
        Deploy a fake execution engine.

        This is used for testing and example purposes.

        :param deploy_name: Deployment name.
        :type deploy_name: str
        :param func: Function to execute.
        :type func: function
        :param f_args: Function arguments.
        :type f_args: tuple
        :return: Fake execution engine deployment
        :rtype: :class:`.FakeDeploy`
        """
        self._deploy = FakeDeploy(
            self._pb_id,
            self._config,
            deploy_name,
            func=func,
            f_args=f_args,
        )
        return self._deploy

    def ee_deploy_helm(
        self,
        deploy_name: str,
        values: Optional[dict] = None,
        version: str = "1.0.0",
    ) -> HelmDeploy:
        """
        Deploy a Helm execution engine.

        This can be used to deploy any Helm chart.

        :param deploy_name: Name of Helm chart.
        :type deploy_name: str
        :param values: Values to pass to Helm chart.
        :type values: dict, optional
        :param version: Helm chart version, defaults to 1.0.0
        :type version: str
        :return: Helm execution engine deployment
        :rtype: :class:`.HelmDeploy`

        """
        self._deploy = HelmDeploy(
            self._pb_id, self._config, deploy_name, values, version
        )
        self._deploy_id_list.append(self._deploy.deploy_id)
        return self._deploy

    def ee_deploy_slurm(
        self, deploy_name: str, slargs: Optional[dict] = None
    ) -> SlurmDeploy:
        """
        Deploy a Slurm execution engine.

        :param deploy_name: name of slurm deployment
        :type deploy_name: str
        :param slargs: slurm arguments
        :type slargs: dict, optional
        :return: Slurm execution engine deployment
        :rtype: :class:`SlurmDeploy`

        """
        self._deploy = SlurmDeploy(
            self._pb_id, self._config, deploy_name, slargs
        )
        self._deploy_id_list.append(self._deploy.deploy_id)
        return self._deploy

    def ee_deploy_dask(
        self, name: str, n_workers: int, func: Callable, f_args: tuple[Any]
    ) -> DaskDeploy:
        """
        Deploy a Dask execution engine.

        :param name: Deployment name.
        :type name: str
        :param n_workers: Number of Dask workers.
        :type n_workers: int
        :param func: Function to execute.
        :type func: callable
        :param f_args: Function arguments.
        :type f_args: tuple
        :return: Dask execution engine deployment
        :rtype: :class:`.DaskDeploy`

        """
        # appending this deployment to self._deploy_id_list list failed
        # because DaskDeploy uses threading, and the deploy_id is not updated
        # until after the code has been executed; this means phase.__exit__
        # fails at deployment clean up;
        # this may need to be addressed in the future
        return DaskDeploy(
            self._pb_id, self._config, name, n_workers, func, f_args
        )

    def is_eb_finished(self, txn: Transaction) -> bool:
        """
        Check if the  :py:class:`ExecutionBlock
        <ska_sdp_config.entity.eb.ExecutionBlock>` is finished or cancelled.

        :param txn: Config db transaction.
        :type txn: :py:class:`Transaction <ska_sdp_config.config.Transaction>`
        :rtype: bool

        """
        eb_state = txn.execution_block.state(self._eb_id).get()
        status = eb_state.get("status")
        if status in DONE_STATES:
            self._status = status
            if status == ExecutionBlockStatus.CANCELLED.value:
                LOG.error("ExecutionBlock is %s", status)
            if self._deploy_id_list:
                self._deploy.update_deploy_status(status)
            return True

        return False

    def update_pb_state(
        self,
        status: ProcessingBlockStatus = ProcessingBlockStatus.UNSET,
    ) -> None:
        """
        Update processing block state.

        If the status is UNSET, it is marked as finished.

        :param status: Status.
        :type status: str, optional

        """
        self._status = status.value

        for txn in self._config.txn():
            # Set state to indicate processing has ended
            state = txn.processing_block.state(self._pb_id).get()
            if self._status is ProcessingBlockStatus.UNSET.value:
                state["status"] = ProcessingBlockStatus.FINISHED.value
            else:
                LOG.info("Setting ProcessingBlock status to %s", self._status)
                state["status"] = self._status
            txn.processing_block.state(self._pb_id).update(state)

    def monitor_deployments(
        self, txn: Transaction, iteration: int = 0
    ) -> None:
        """
        Monitor deployments, update the deployment status
        in the processing block state based on the deployments'
        pods' state.

        Also update the deployments_ready pb state key.

        At the moment deployments_ready = True only if all
        deployments are RUNNING. Else, it is False.

        :param txn: Transaction object.
        :type txn: :py:class:`Transaction <ska_sdp_config.config.Transaction>`
        :param iteration: Number of txn iteration.
        :type iteration: int
        """
        LOG.info("Checking deployment status (attempt %s)", iteration)
        pb_state = txn.processing_block.state(self._pb_id).get()
        pb_state_original = deepcopy(pb_state)

        if not self._deploy_id_list:
            if self._script_kind == "realtime":
                pb_state["deployments_ready"] = True

            if pb_state != pb_state_original:
                LOG.info(
                    "Updating processing block state (attempt %s)", iteration
                )
                txn.processing_block.state(self._pb_id).update(pb_state)
            return

        if "error_messages" not in pb_state:
            pb_state["error_messages"] = {}

        # collect the states of deployments for conditional testing
        deployments = pb_state.get("deployments")
        deployment_states = []
        for deploy_id in self._deploy_id_list:
            deploy_status = self._get_deployment_status(txn, deploy_id)
            LOG.info(
                "State of deployment %s is %s (attempt %s)",
                deploy_id,
                deploy_status["status"],
                iteration,
            )
            deployment_states.append(deploy_status)

            if deploy_status["status"] is None:
                continue

            # we are here if we have a deployment
            deployments[deploy_id] = deploy_status["status"]
            error_message = deploy_status.get("error_state")

            # add new error message to deployment entry
            if error_message is not None:
                pb_state["error_messages"][deploy_id] = error_message

            # clear deployment's errors if they no longer exist
            elif deploy_id in pb_state["error_messages"]:
                del pb_state["error_messages"][deploy_id]

        # set deployments state
        pb_state["deployments"] = deployments

        # set pb state status to FAILED if any deployments failed
        states = {state["status"] for state in deployment_states}
        if DeploymentStatus.FAILED in states:
            pb_state["status"] = "FAILED"

        # Set readiness state if all deployments are live and kind is realtime
        if self._script_kind == "realtime":
            pb_state["deployments_ready"] = states == {
                DeploymentStatus.RUNNING
            }

        if pb_state != pb_state_original:
            LOG.info("Updating processing block state (attempt %s)", iteration)
            txn.processing_block.state(self._pb_id).update(pb_state)

    @staticmethod
    def _evaluate_deployment_state(status_list, error_list):
        """
        Evaluates and returns the deployment state based
        on the status list.
        Default to UNSET and only set it to FINISHED if
        all statuses indicate completion.
        """
        state = {"status": DeploymentStatus.UNSET.value}

        if DeploymentStatus.PENDING.value in status_list:
            state["status"] = DeploymentStatus.PENDING.value
        elif DeploymentStatus.FAILED.value in status_list:
            state["status"] = DeploymentStatus.FAILED.value
            state["error_state"] = error_list
        elif DeploymentStatus.CANCELLED.value in status_list:
            state["status"] = DeploymentStatus.CANCELLED.value
        elif DeploymentStatus.RUNNING.value in status_list:
            state["status"] = DeploymentStatus.RUNNING.value
        elif all(
            status == DeploymentStatus.FINISHED.value for status in status_list
        ):
            state["status"] = DeploymentStatus.FINISHED.value

        return state

    @staticmethod
    def _get_deployment_status(
        txn: Transaction, deploy_id: str
    ) -> Optional[dict]:
        """
        Return the deployments' state depending on
        the status of their contributing pods.
        """
        current_deploy_state = txn.deployment.state(deploy_id).get()
        if current_deploy_state is None:
            return {"status": None}

        # Detect if it's a Slurm deployment
        is_slurm = "num_job" in current_deploy_state

        if is_slurm:
            # Only track errors for Slurm, not general status updates
            num_job = current_deploy_state.get("num_job", 0)
            deploy_state_jobs = current_deploy_state.get("jobs", {})

            if (
                not deploy_state_jobs
                or len(deploy_state_jobs.keys()) < num_job
            ):
                return {"status": DeploymentStatus.PENDING.value}

            job_status_list = [
                job.get("status") for job in deploy_state_jobs.values()
            ]
            job_errors = [
                job.get("error_state", "Unknown error")
                for job in deploy_state_jobs.values()
                if job.get("status") == DeploymentStatus.FAILED.value
            ]
            return Phase._evaluate_deployment_state(
                job_status_list, job_errors
            )

        # Kubernetes deployment
        num_pod = current_deploy_state.get("num_pod", 0)
        deploy_state_pods = current_deploy_state.get("pods", None)

        if not deploy_state_pods or len(deploy_state_pods.keys()) < num_pod:
            return {"status": DeploymentStatus.PENDING.value}

        pod_status_list = [pod["status"] for pod in deploy_state_pods.values()]
        pod_errors = [
            pod.get("error_state")
            for pod in deploy_state_pods.values()
            if pod["status"] == DeploymentStatus.FAILED.value
        ]
        return Phase._evaluate_deployment_state(pod_status_list, pod_errors)

    def monitor_data_flows(self, txn: Transaction, iteration: int = 0) -> None:
        """
        Monitor the data flow state and aggregate any error messages
        into the processing block's `'error_messages'` key.

        :param txn: Transaction object.
        :type txn: :py:class:`Transaction <ska_sdp_config.config.Transaction>`
        :param iteration: Number of txn iteration.
        :type iteration: int
        """
        pb_state = txn.processing_block.state(self._pb_id).get()
        pb_state_original = deepcopy(pb_state)

        if "error_messages" not in pb_state:
            pb_state["error_messages"] = {}

        for data_flow in self.flows:
            LOG.info("Checking data flow status (attempt %s)", iteration)
            data_flow_key = data_flow.flow.key
            data_flow_state = txn.flow.state(data_flow_key).get()
            kind = data_flow_key.kind

            LOG.info(
                "State of data flow %s is %s (attempt %s)",
                kind,
                data_flow_state["status"],
                iteration,
            )
            if data_flow_state is None:
                continue

            # Check for errors in the data flow state and collect info
            if error_log := data_flow_state.get("log"):
                # update pb state
                pb_state["error_messages"][kind] = {
                    "key": data_flow_key.dict(),
                    "error_message": error_log,
                }

            # Remove any existing error message for this flow if they no
            # longer exist
            elif kind in pb_state["error_messages"]:
                del pb_state["error_messages"][kind]

        if pb_state != pb_state_original:
            LOG.info(
                "Updating processing block state (attempt %s)",
                iteration,
            )
            txn.processing_block.state(self._pb_id).update(pb_state)

    def wait_loop(
        self, func: Callable[[Transaction], bool], time_to_ready: int = 0
    ) -> None:
        """
        Wait loop to check the status of the processing block. It also updates
        the processing block state with deployment statuses and errors captured
        in data flows for realtime scripts.

        :param func: Function to check condition for exiting the watcher loop.
        :param time_to_ready: Set deployments_ready to true after this amount
          of time has passed (seconds). Only for deployments deployed with
          :meth:`.ee_deploy_test`.
        """
        condition_met = False
        for watcher in self._config.watcher():
            for attempt, txn in enumerate(watcher.txn()):
                self.check_state(txn, check_realtime=False)

                self.monitor_deployments(txn, attempt)
                self.monitor_data_flows(txn, attempt)

                if isinstance(self._deploy, FakeDeploy):
                    self._deploy.set_deployments_ready(
                        time_to_ready=time_to_ready
                    )

                condition_met = func(txn)

            if condition_met:
                break
