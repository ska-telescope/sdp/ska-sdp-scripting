"""
Generate Flow data to be written into the Config DB.
"""

from __future__ import annotations

from typing import Union

from ska_sdp_config.entity import Flow
from ska_sdp_config.entity.flow import (
    DataProduct,
    DataQueue,
    Display,
    FlowSource,
    SharedMem,
    SpeadStream,
    TangoAttribute,
    TangoAttributeMap,
    TelModel,
)

# pylint: disable-next=invalid-name
FLOW_SINKS = Union[
    TelModel,
    SharedMem,
    DataQueue,
    Display,
    DataProduct,
    TangoAttribute,
    TangoAttributeMap,
    SpeadStream,
]


# pylint: disable-next=too-few-public-methods
class DataFlow:
    """
    Data flow class to generate and validate
    Flow objects to be written into the Configuration DB.

    This should not be created directly, use the
    :func:`ProcessingBlock` methods instead.

    :param pb_id: processing block ID
    :param name: unique name of the data flow object for this kind
    :param data_model: data model the Flow data represents
    :param sink: sink that will use the data
    :param sources: source that will generate the data
                    default: []
    """

    def __init__(
        self,
        pb_id: str,
        name: str,
        data_model: str,
        sink: FLOW_SINKS,
        sources: list | None = None,
    ):
        self.flow = Flow(
            key=Flow.Key(
                pb_id=pb_id,
                name=name,
            ),
            sink=sink,
            sources=sources if sources is not None else [],
            data_model=data_model,
        )

    def add_source(
        self,
        source: FlowSource | DataFlow | dict,
        function=None,
        parameters=None,
    ):
        """
        Add sources to the Flow object.

        Accepted input source definitions:
        - a user-defined FlowSource object
        - a previously created DataFlow object
        - a dict that contains the uri. It cannot have other keys but "uri".
        """
        match source:
            case FlowSource():
                self.flow.sources.append(source)
            case DataFlow():
                flow_source = FlowSource(
                    uri=source.flow.key,
                    function=function,
                    parameters=parameters,
                )
                self.flow.sources.append(flow_source)
            case dict():
                match list(source.keys()):
                    case ["uri"]:
                        flow_source = FlowSource(
                            uri=source["uri"],
                            function=function,
                            parameters=parameters,
                        )
                        self.flow.sources.append(flow_source)
                    case _:
                        raise ValueError(
                            "Incorrect information given in "
                            "user-defined 'source' dictionary."
                            "'uri' is missing."
                        )
            case _:
                raise ValueError(
                    f"Provided source type, {type(source)}, is not supported."
                )
