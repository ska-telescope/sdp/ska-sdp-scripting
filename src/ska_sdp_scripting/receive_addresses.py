"""Generate Receive Addresses"""

import logging

LOG = logging.getLogger("ska_sdp_scripting")


def generate_host_port_channel_map(
    scan_types: list,
    port_start: int,
    channels_per_port: int,
    *,
    num_hosts: int = 1,
    max_ports_per_host: int | None = None,
) -> tuple[dict, dict]:
    """
    Returns the dictionary of host and port channel maps for a
    given list of scan types. The dictionary is indexed by Scan Type ID
    and then by Beam ID, each entry at this last level has two lists under
    "host" and "port" with the corresponding channel maps.

    :param scan_types: The list of scan types for which receive addresses
      should be calculated.
    :param port_start: The first port to allocate on each host.
    :param channels_per_port: The number of channels that are sent
      per port.
    :param num_hosts: If given, the exact number of hosts to allocate.
    :param max_ports_per_host: If given, the maximum number of ports to
      allocate to a host before a new host is allocated (per beam).
    :returns: The dictionary of host and port channels maps for a given
      list of scan types. Hosts are identified by running, 0-indexed
      numbers, so that actual hostnames/IPs can be easily assigned later.
    """

    beam_recv_address_info = {
        scan_type["scan_type_id"]: {
            beam_id: generate_host_port_per_beam(
                beam["spectral_windows"],
                port_start,
                channels_per_port,
                num_hosts=num_hosts,
                max_ports_per_host=max_ports_per_host,
            )
            for beam_id, beam in scan_type["beams"].items()
        }
        for scan_type in scan_types
    }
    recv_addresses = {
        scan_type_id: {
            beam_id: {"host": beam_value[0], "port": beam_value[1]}
            for beam_id, beam_value in scan_value.items()
        }
        for scan_type_id, scan_value in beam_recv_address_info.items()
    }
    host_port_count = {
        scan_type_id: {
            beam_id: beam_value[2]
            for beam_id, beam_value in scan_value.items()
        }
        for scan_type_id, scan_value in beam_recv_address_info.items()
    }
    return recv_addresses, host_port_count


# pylint: disable-next=too-many-locals
def generate_host_port_per_beam(
    spectral_windows: list,
    port_start: int,
    channels_per_port: int,
    *,
    num_hosts: int | None = None,
    max_ports_per_host: int | None = None,
) -> tuple[list, list, list]:
    """
    Returns the list of host and port channel maps for a given beam's spectral
    window list. The calculation starts at a given starting port for
    convenience and takes into account that more than one channel can be sent
    per port. Additionally, users can specify either a fixed number of pods
    that the algorithm should allocate, or (alternatively) the maximum number
    of ports after which a new pod should be allocated by the algorithm.

    :param spectral_windows: The list of Spectral Windows for which receive
      addresses should be calculated.
    :param port_start: The first port to allocate on each host.
    :param channels_per_port: The number of channels that are sent per port.
    :param num_hosts: If given, the exact number of hosts to allocate.
    :param max_ports_per_host: If given, the maximum number of ports to
      allocate to a host before a new host is allocated.
    :returns: A three-tuple with: the list of host channel maps, the list of
      port channel maps, and a list of port counts for each host. Hosts are
      identified by running, 0-indexed numbers, so that actual hostnames/IPs
      can be easily assigned later.
    """
    assert num_hosts or max_ports_per_host

    # The algorithm below assumes SWs are sorted by starting channel ID
    spectral_windows = sorted(spectral_windows, key=lambda sw: sw["start"])

    # We are commanded to allocate a given number of hosts, let's calculate the
    # value for max_ports_per_host based that spreads ports as equally as
    # possible
    if num_hosts:
        total_number_of_channels = sum(sw["count"] for sw in spectral_windows)
        total_number_of_ports = sum(
            _ceil_div(sw["count"], channels_per_port)
            for sw in spectral_windows
        )
        max_ports_per_host = _ceil_div(total_number_of_ports, num_hosts)
        LOG.info(
            "Given num_hosts=%d constraint translated to max_ports_per_host=%d"
            " to cover %d ports (%d channels)",
            num_hosts,
            max_ports_per_host,
            total_number_of_ports,
            total_number_of_channels,
        )

    hosts = [(spectral_windows[0]["start"], 0)]
    ports = []
    host_port_counts = [0]
    for sw in spectral_windows:
        start = sw["start"]
        count = sw["count"]
        stride = sw.get("stride") or 1
        channel_ids_to_allocate = list(
            range(start, start + count * stride, stride)
        )
        while channel_ids_to_allocate:
            current_host_port_count = host_port_counts[-1]
            ports_to_allocate = _ceil_div(
                len(channel_ids_to_allocate), channels_per_port
            )
            port_allocation_size = min(
                max_ports_per_host - current_host_port_count, ports_to_allocate
            )
            port_allocation_start = port_start + current_host_port_count
            start_channel_id = channel_ids_to_allocate[0]
            if port_allocation_size == 0:
                # A new host needs to be allocated, do that and try again, it
                # will now succeed
                host_port_counts.append(0)
                hosts.append((start_channel_id, len(hosts)))
                continue

            if channels_per_port == 1 and stride == 1:
                ports.append((start_channel_id, port_allocation_start, 1))
            else:
                ports += [
                    (
                        start_channel_id + i * channels_per_port * stride,
                        port_allocation_start + i,
                    )
                    for i in range(port_allocation_size)
                ]
            host_port_counts[-1] += port_allocation_size

            del channel_ids_to_allocate[
                : port_allocation_size * channels_per_port
            ]

    assert all(
        port_count <= max_ports_per_host for port_count in host_port_counts
    )
    return hosts, ports, host_port_counts


def _ceil_div(x: float, y: float) -> float:
    """
    Returns the result of dividing x by y

    :param x: numerator.
    :param y: denominator.

    :returns: the quotient of x divided by y.
    """
    return (x + y - 1) // y
