"""
This module defines a high-level API for SKA SDP (Science Data Processor)
scripts. It simplifies the process of claiming processing blocks, managing
their state, handling input parameters, defining data flows, and creating
execution phases for the processing block. The API interacts with the SDP
configuration database to manage the state and configuration of processing
blocks.
"""

import logging
import os
import traceback
from collections.abc import Mapping, Sequence
from copy import deepcopy
from datetime import datetime

import ska_ser_logging
from pydantic import BaseModel, ConfigDict, ValidationError

from .buffer_request import BufferRequest
from .config import new_config_client
from .data_flow import FLOW_SINKS, DataFlow
from .phase import Phase
from .receive_addresses import generate_host_port_channel_map

# Initialise logging
ska_ser_logging.configure_logging()
LOG = logging.getLogger("ska_sdp_scripting")
LOG.setLevel(logging.DEBUG)


class ParameterBaseModel(BaseModel):
    """
    Base class for all script parameter models.
    New script models should derive from this.
    """

    model_config = ConfigDict(
        strict=True,
        extra="forbid",
        arbitrary_types_allowed=False,
        validate_assignment=True,
    )


# pylint: disable-next=too-many-instance-attributes
class ProcessingBlock:
    """
    This is the core class of the API. It handles the following processes:

    - **Claiming and Releasing Processing Blocks:**
        The constructor claims a processing block identified by
        :paramref:`pb_id` from the configuration database. The exit method (and
        the context manager protocol via __enter__ and __exit__) releases the
        claim.
    - **Managing Processing Block State:**
        The :py:meth:`receive_addresses` method update the state of the
        processing block in the configuration database, including the receive
        addresses.
    - **Handling Script Parameters:**
        Methods :py:meth:`get_parameters`, :py:meth:`update_parameters`, and
        :py:meth:`validate_parameters` manage script parameters, including
        validation against a provided Pydantic model.
    - **Defining Data Flows:**
        The :py:meth:`create_data_flow` method allows defining data flows
        within the processing block, specifying source and sink types.
    - **Creating Execution Phases:**
        The :py:meth:`create_phase` method creates execution phases, which
        represent steps in the processing block, and associates them with
        resource requests (like buffer reservations).
    - **Retrieving Dependencies and Scan Types:**
        :py:meth:`get_dependencies` retrieves the dependencies of the
        processing block, and :py:meth:`get_scan_types` retrieves and updates
        scan types from the associated execution block(s).

    :param pb_id: Processing block ID.
    :type pb_id: str, optional
    """

    def __init__(self, pb_id: str = None):
        # Get connection to config DB
        LOG.info("Opening connection to config DB")
        self._config = new_config_client()

        # Processing block ID
        self._pb_id = pb_id or os.getenv("SDP_PB_ID")
        LOG.debug("Processing Block ID %s", self._pb_id)

        # Claim processing block
        pb = None
        for txn in self._config.txn():
            txn.processing_block.ownership(self._pb_id).take()
            pb = txn.processing_block.get(self._pb_id)
        LOG.info("Claimed processing block")

        # Processing Block
        self._pb = pb

        # Execution block ID
        self._eb_id = pb.eb_id

        # DNS name
        self._service_name = "receive"
        self._chart_name = None
        self._namespace = None

        # Ports
        self._ports = []

        # Data flow objects
        self._flows = []

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):

        self._handle_exception(exc_type, exc_val, exc_tb)

        LOG.info("Closing connection to config DB")
        self._config.close()

    def get_dependencies(self) -> list:
        """
        Get the list of processing block dependencies.

        :returns: Processing block dependencies.
        :rtype: list
        """
        return self._pb.dependencies

    def get_parameters(self) -> dict:
        """
        Get script parameters from processing block.
        The schema checking is not currently implemented.

        :returns: Processing block parameters.
        :rtype: dict
        """
        return self._pb.parameters

    def update_parameters(
        self, default_parameters: dict, parameters: dict | Mapping
    ) -> dict:
        """
        Overwrite (nested) values in `default_parameter` mapping with those
        from `parameters` mapping.

        :param default_parameters: Default parameter values.
        :type default_parameters: dict
        :param parameters: Script specific parameters.
        :type parameters: dict
        :returns: Processing block additional parameters.
        :rtype: dict
        """
        for param, value in parameters.items():
            if isinstance(value, Mapping):
                default_parameters[param] = self.update_parameters(
                    default_parameters.get(param, {}), value
                )
            else:
                default_parameters[param] = value
        return default_parameters

    def validate_parameters(self, model):
        """
        Validate processing block parameters against a given Pydantic model.

        Raises Validation error if validation fails else returns the Pydantic
        class initialized with the processing block parameters.

        :parameter model: Model to validate against
        :type model: Pydantic model
        :returns: Parameters
        :rtype: :class:`.ParameterBaseModel`
        :raises: :exc:`ValidationError`
        """
        parameters = self.get_parameters()
        try:
            param_class = model(**parameters)
            LOG.info("Successfully validated processing block parameters.")
            return param_class

        except ValidationError:
            LOG.error("Processing block parameter validation failed.")
            raise

    def get_scan_types(self) -> list[dict]:
        """
        Get scan types from the execution block.
        Updates the scan types with the default parameters and channels.
        This is only supported for real-time scripts

        :returns: Scan types.
        :rtype: list
        """
        LOG.info("Retrieving channel link map from ExecutionBlock")
        concrete_scan_types = None

        for txn in self._config.txn():
            eb = txn.execution_block.get(self._eb_id)
            scan_types = eb.scan_types
            channels = eb.channels
            default_scan_types = {}
            concrete_scan_types = []

            # collect pseudo scan types containing default values
            for scan_type in scan_types:
                scan_type_id = scan_type.get("scan_type_id")
                if scan_type_id.startswith("."):
                    default_scan_types[scan_type_id] = scan_type
                else:
                    concrete_scan_types.append(scan_type)

            self._update_scan_types(
                concrete_scan_types, default_scan_types, channels
            )

        # scan_types are updated in place
        return concrete_scan_types

    def receive_addresses(
        self,
        configured_host_port: dict,
        chart_name: str | None = None,
        service_name: str | None = None,
        namespace: str | None = None,
        update_dns: bool = True,
    ) -> None:
        """
        Generate receive addresses and update the processing block state.
        Chart_name and service_name are only needed if the
        :paramref:`configured_host_port` needs updating with DNS names
        Currently this only applies to the vis-receive script.

        :param configured_host_port: Constructed host and port.
        :type configured_host_port: dict
        :param chart_name: Name of the statefulset.
        :type chart_name: str
        :param service_name: Name of the headless service.
        :type service_name: str
        :param namespace: Namespace where it's going to be deployed.
        :type namespace: str
        :param update_dns: Whether to update with DNS address, True by default.
        :type update_dns: boolean

        """
        # Generate receive addresses
        LOG.info("Generating receive addresses")

        if update_dns:
            # Update the DNS address within the host port
            # Using names of the service and chart
            self._update_chart_names(service_name, chart_name, namespace)
            receive_addresses = self._update_with_dns_name(
                configured_host_port
            )
            LOG.info("Updated host port with DNS names.")
        else:
            # we do not update anything
            # This is mainly for the pointing script
            LOG.info("No update of DNS names executed.")
            receive_addresses = configured_host_port

        self._add_config_beam_info_to_recv_addrs(receive_addresses)

        # Update receive addresses in processing block state
        # ExecutionBlock does not need to be updated
        LOG.info("Updating receive addresses in processing block state")
        for txn in self._config.txn():
            state = txn.processing_block.state(self._pb_id).get()
            state["receive_addresses"] = receive_addresses
            txn.processing_block.state(self._pb_id).update(state)

    @staticmethod
    def config_host_port_channel_map(
        scan_types: list[dict],
        port_start: int,
        channels_per_port: int,
        *,
        num_hosts: int = 1,
        max_ports_per_host: int | None = None,
    ) -> tuple[dict, dict]:
        """
        Configures a dictionary of host and port channel maps for a given list
        of scan types. This is a wrapper function for
        :py:func:`.receive_addresses.generate_host_port_channel_map` function
        which returns a dictionary of host and port channel maps for a given
        list of scan types.

        :param scan_types: The list of scan types for which receive addresses
          should be calculated.
        :type scan_types: list[dict]
        :param int port_start: The first port to allocate on each host.
        :param int channels_per_port: The number of channels that are sent per
          port.
        :param int num_hosts: If given, the exact number of hosts to allocate.
        :param int max_ports_per_host: If given, the maximum number of ports to
          allocate to a host before a new host is allocated (per beam).
        :returns: Two dictionaries keyed on the scan type id, one for hosts and
          one for ports of the channel maps for a given list of scan types.
          Hosts are identified by running, 0-indexed numbers, so that actual
          hostnames/IPs can be easily assigned later.
        :rtype: tuple[dict, dict]
        """

        recv_addresses, host_port_count = generate_host_port_channel_map(
            scan_types,
            port_start,
            channels_per_port,
            num_hosts=num_hosts,
            max_ports_per_host=max_ports_per_host,
        )
        return recv_addresses, host_port_count

    def create_data_flow(
        self,
        name: str,
        data_model: str,
        sink: FLOW_SINKS,
        sources: list | None = None,
    ) -> DataFlow:
        """
        Create and register a DataFlow to this processing block.

        :param name: Name of the DataFlow object.
        :type name: str
        :param data_model: Data model.
        :type data_model: str
        :param sink: Sink used in this flow.
        :type sink: see SINKS in data_flow.py for available options
        :param sources: List of sources to be used.
        :type sources: list or None

        :returns: Data flow.
        :rtype: :py:class:`.dataflow.DataFlow`
        """
        flow = DataFlow(self._pb_id, name, data_model, sink, sources=sources)
        self._flows.append(flow)
        return flow

    @staticmethod
    def request_buffer(size: float, tags: list[str]) -> BufferRequest:
        """
        Request a buffer reservation.

        This returns a buffer reservation request that is used to create a
        script phase. These are currently only placeholders.

        :param size: Size of the buffer.
        :type size: float
        :param tags: Tags describing the type of buffer required.
        :type tags: list of str
        :returns: Buffer reservation request.
        :rtype: :class:`.BufferRequest`

        """
        return BufferRequest(size, tags)

    def create_phase(self, name: str, requests: list[BufferRequest]) -> Phase:
        """
        Create a script phase for deploying execution engines.

        The phase is created with a list of resource requests which must be
        satisfied before the phase can start executing. For the time being the
        only resource requests are (placeholder) buffer reservations, but
        eventually this will include compute requests too.

        :param name: Name of the phase.
        :type name: str
        :param requests: Resource requests.
        :type requests: list of :py:class:`.BufferRequest`.
        :returns: The script phase object.
        :rtype: :class:`.phase.Phase`

        """
        kind = self._pb.script.kind

        return Phase(
            name,
            requests,
            self._config,
            self._pb_id,
            self._eb_id,
            kind,
            self._flows,
        )

    # -------------------------------------
    # Private methods
    # -------------------------------------
    def _handle_exception(self, exc_type, exc_val, exc_tb):
        """
        Update processing block state with application level exception details.

        :param exc_type: Exception type.
        :type exc_type: Exception
        :param exc_val: Exception message.
        :type exc_val: str
        :param exc_tb: Tracebock message.
        :param exc_tb: Str.
        """
        for txn in self._config.txn():
            pb_state = txn.processing_block.state(self._pb_id).get()

            if pb_state is None:
                return

            pb_state_original = deepcopy(pb_state)

            if "error_messages" not in pb_state:
                pb_state["error_messages"] = {}

            if exc_type is not None:
                error_message = {
                    "type": exc_type.__name__,
                    "message": str(exc_val),
                    "time": str(datetime.now()),
                    "traceback": "".join(traceback.format_tb(exc_tb)),
                }

                pb_state["error_messages"]["script_error"] = error_message
                pb_state["status"] = "FAILED"

                LOG.error(
                    "An application level exception occurred: %s",
                    error_message,
                )

            # remove resolved error messages
            elif "script_error" in pb_state["error_messages"]:
                del pb_state["error_messages"]["script_error"]

            if pb_state != pb_state_original:
                LOG.info("Updating processing block state")
                txn.processing_block.state(self._pb_id).update(pb_state)

    def _update_scan_types(
        self,
        concrete_scan_types: list[dict],
        default_scan_types: dict,
        channels: dict,
    ) -> None:
        """
        Updates the scan types with the default parameters and channels.
        Factored out from `get_scan_types` to reduce complexity.

        :param concrete_scan_types: Concrete (non-default) scan types.
        :type concrete_scan_types: list
        :param default_scan_types: Default scan types.
        :type default_scan_types: dict
        :param channels: Channel definitions.
        :type channels: dict

        """

        for scan_type in concrete_scan_types:
            beams = scan_type.get("beams")

            # Updates the scan type if required to derive
            # from default scan types
            if "derive_from" in scan_type.keys():
                default = default_scan_types[scan_type.get("derive_from")]
                beams = {
                    **{k: {} for k in default.get("beams").keys()},
                    **beams,
                }
                for beam_key, beam_value in beams.items():
                    if beam_key in default.get("beams"):
                        beam_value.update(default.get("beams")[beam_key])
                scan_type["beams"] = beams

            # Update scan types with relevant channels to the beams
            self._add_channels(channels, scan_type)

    def _update_chart_names(
        self,
        service_name: str | None,
        chart_name: str | None,
        namespace: str | None,
    ) -> None:
        """
        Assign custom names if they are provided by user
        This is required for _update_with_dns_name.
        Note: the namespace is read from
        the "SDP_HELM_NAMESPACE" env variable if not provided

        :param chart_name: Name of the statefulset.
        :param service_name: Name of the headless service.
        :param namespace: Namespace where it's going to be deployed.
        :return: updated names stored in :py:class:`.ProcessingBlock`
        """

        self._namespace = namespace or os.getenv("SDP_HELM_NAMESPACE")

        if service_name is not None:
            self._service_name = service_name

        if chart_name is None:
            for txn in self._config.txn():
                for deploy_id in txn.deployment.list_keys():
                    if self._pb_id in deploy_id:
                        self._chart_name = deploy_id
        else:
            self._chart_name = chart_name

    def _update_with_dns_name(self, configured_host_port: dict) -> dict:
        """
        Generate DNS name for the receiving processes. The DNS address will
        then be updated within the host port This only needs to be done for
        receive scripts.

        The host part of the DNS name is generated by the
        :py:class:`ProcessingBlock` for each host in the host maps by doing
        "<deployment_id><host_stub>", with <host_stub> being the current host
        value in the host maps. At this point <host_stub> contains the numbers
        0, 1, 2..., so we now turn them into the correct prefix that, after
        appended to the deployment ID, will yield the pod names as generated by
        the vis-receive chart (which have the form
        "<deployment_id>-<statefulset_index:02d>-0"). Note also that the host
        needs to have a dot at the end because the :py:class:`ProcessingBlock`
        appends the service name straight after the hostname.

        :param configured_host_port: Constructed host and port.
        :return: configured_host_port (input dictionary modified in-place).
        """

        if None in (self._chart_name, self._service_name, self._namespace):
            raise ValueError(
                "Names of chart, service and namespace "
                "must be provided to obtain receive address DNS name"
            )

        # If the correct names are provided
        # Get DNS address
        for beam in configured_host_port.values():
            for values in beam.values():
                # Create a new list to store modified hosts
                starts, hostnames = zip(*values["host"])
                modified_hosts = (
                    f"{self._chart_name}"
                    f"-{int(hostname):02d}-0"
                    f".{self._service_name}"
                    f".{self._namespace}"
                    for hostname in hostnames
                )
                values["host"] = list(zip(starts, modified_hosts))

        return configured_host_port

    @staticmethod
    def _add_channels(channels: dict, scan_type: dict) -> None:
        """
        Add related channels to the beams in the given scan_type

        :param channels: Channels from execution block.
        :param scan_type: Scan type to update.
        """
        beams = scan_type.get("beams")
        for beam, beam_value in beams.items():
            beam_channels_id = beam_value.get("channels_id")
            for channel in channels:
                if beam_channels_id == channel.get("channels_id"):
                    beam_value.update(channel)
                    break

            if beam_channels_id and "spectral_windows" not in beam_value:
                raise ValueError(
                    f"scan_type with {beam} beam does not "
                    f"have channel_id information."
                )

    def _add_config_beam_info_to_recv_addrs(
        self, receive_addresses: dict
    ) -> None:
        """
        Add the "function", and if present the "{x}_beam_id" keys from
        "beams" listed in the execution block to the appropriate
        beams in receive_addresses.

        "{x}_beam_id" can be
        visibility_beam_id, search_beam_id, timing_beam_id, vlbi_beam_id
        depending on the type of beam.

        Updates receive_addresses in place.
        """
        beam_id_types = ["visibility", "search", "timing", "vlbi"]

        for txn in self._config.txn():
            eb = txn.execution_block.get(self._eb_id)
            eb_beams_dict = {beam["beam_id"]: beam for beam in eb.beams}

            for recv_beam in receive_addresses.values():
                self._add_recv_beam(recv_beam, eb_beams_dict, beam_id_types)

    @staticmethod
    def _add_recv_beam(
        recv_beam: dict, eb_beams: dict, beam_id_types: Sequence[str]
    ) -> None:
        """
        Factored out from the above to reduce complexity,
        see that for description (each receive address).

        :param recv_beam: Receive address value.
        :param eb_beams: Mapping of ids to beams.
        :param beam_id_types: The types of beam.
        """
        for beam_id, beam_value in recv_beam.items():
            if "function" in eb_beams[beam_id].keys():
                beam_value["function"] = eb_beams[beam_id]["function"]

            for int_beam_id in beam_id_types:
                int_beam_id_key = f"{int_beam_id}_beam_id"
                if int_beam_id_key in eb_beams[beam_id].keys():
                    beam_value[int_beam_id_key] = eb_beams[beam_id][
                        int_beam_id_key
                    ]
