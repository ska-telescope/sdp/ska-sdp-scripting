"""
Utils
"""

import enum


@enum.unique
class ProcessingBlockStatus(str, enum.Enum):
    """
    Status values for processing block.
    """

    WAITING = "WAITING"
    READY = "READY"
    RUNNING = "RUNNING"
    FINISHED = "FINISHED"
    CANCELLED = "CANCELLED"
    UNSET = "UNSET"
    FAILED = "FAILED"


@enum.unique
class ExecutionBlockStatus(str, enum.Enum):
    """
    Status values execution block.
    """

    ACTIVE = "ACTIVE"
    FINISHED = "FINISHED"
    CANCELLED = "CANCELLED"
    UNSET = "UNSET"


@enum.unique
class DeploymentStatus(str, enum.Enum):
    """
    Status values for deployment.
    """

    PENDING = "PENDING"
    RUNNING = "RUNNING"
    FINISHED = "FINISHED"
    CANCELLED = "CANCELLED"
    FAILED = "FAILED"
    UNSET = "UNSET"


@enum.unique
class FlowStatus(str, enum.Enum):
    """
    Status values for data flows.
    """

    PENDING = "PENDING"
    FLOWING = "FLOWING"
    FAULT = "FAULT"
