"""Slurm Deploy class module for SDP scripting."""

import logging

from ska_sdp_config import Config, Deployment

from .ee_base_deploy import EEDeploy
from .utils import DeploymentStatus

LOG = logging.getLogger("ska_sdp_scripting")


class SlurmDeploy(EEDeploy):
    """
    Deploy Slurm execution engine.

    This should not be created directly, use the :func:`Phase.ee_deploy_slurm`
    method instead.

    :param pb_id: processing block ID
    :type pb_id: str
    :param config: SDP configuration client
    :type config: ska_sdp_config.Config
    :param deploy_name: deployment name
    :type deploy_name: str
    :param slargs: slurm arguments to pass
    :type slargs: dict, optional
    """

    def __init__(
        self, pb_id: str, config: Config, deploy_name: str, slargs: dict = None
    ):
        super().__init__(pb_id, config)
        self._deploy(deploy_name, slargs)

    def _deploy(self, deploy_name: str, slargs: dict = None) -> None:
        """
        Deploy the Slurm execution engine.

        :param deploy_name: deployment name
        :param slargs: optional dict of slurm arguments

        """
        LOG.info("Deploying Slurm: %s", deploy_name)
        self._deploy_id = f"proc-{self._pb_id}-{deploy_name}"

        # upon creation, the deployment status in processing
        # block state should always start with PENDING
        self.update_deploy_status(DeploymentStatus.PENDING.value)

        args = {}
        if slargs is not None and isinstance(slargs, dict):
            args.update(slargs)

        deploy = Deployment(key=self._deploy_id, kind="slurm", args=args)
        for txn in self._config.txn():
            txn.deployment.create(deploy)
