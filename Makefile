include .make/base.mk
include .make/python.mk
include .make/dependencies.mk

PROJECT_NAME = ska-sdp-scripting
PROJECT_PATH = ska-telescope/sdp/ska-sdp-scripting
ARTEFACT_TYPE = python
CHANGELOG_FILE = CHANGELOG.rst

# W503: line break before binary operator
PYTHON_SWITCHES_FOR_FLAKE8 = --ignore=W503
