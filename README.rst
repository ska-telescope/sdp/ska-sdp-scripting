SDP Scripting Library
=====================

Introduction
------------

The SDP scripting library is a high-level interface for writing
processing scripts. Its goal is to provide abstractions to enable the
developer to express the high-level organisation of a processing script
without needing to interact directly with the low-level interfaces such
as the SDP configuration library.

Standard CI machinery
---------------------

The repository is set up to use `CI
templates <https://gitlab.com/ska-telescope/templates-repository>`__ and
`makefiles <https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile>`__
maintained by the System Team.

For any questions, please look at the repositories’ documentation or ask
for support on Slack in the #team-system-support channel.

To keep the makefile modules up to date locally, follow the instructions
at:
https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile#keeping-up-to-date

Creating a new release
----------------------

When new release is ready:

- checkout the master branch
- bump the ``.release`` file version (also updates pyproject.toml and
  ``docs/src/conf.py``) with

  - ``make bump-patch-release``
  - ``make bump-minor-release``, or
  - ``make bump-major-release``

- update the ``CHANGELOG.rst``
- create the git tag with ``make create-git-tag``; use your ORCA ticket
  when it asks for a JIRA ticket ID
- push the changes using ``make push-git-tag``
