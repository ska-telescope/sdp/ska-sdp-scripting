Changelog
=========

Development
-----------

- Add capability to monitor slurm-type deployments
  (`MR95 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/95>`__)

1.0.0
-----

- Remove code to deal with earlier queue connector versions before v5
  (`MR94 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/94>`__)
- phase.ScriptPhaseException is renamed to phase.PhaseException
  (`MR88 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/88>`__)
- EEDeploy.get_id() replaced with deploy_id property
  (`MR88 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/88>`__)
- Updated dependencies (for list, see pyproject.toml on the MR;
  `MR88 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/88>`__)
- Added slurm deploy capability
  (`MR89 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/89>`__)

0.12.0
------

- Allow for configuring the queue connector depending on its version
  (`MR86 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/86>`__):

  - if major version 5 or larger: configure via flows
  - if major version 4 or lower: configure via exchanges
  - Note:
    `MR85 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/85>`__
    removed configuring via exchanges, but
    `MR86 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/86>`__
    added it back for short-term backwards compatibility

- PB status set to FAILED if deployment fails or processing script pod
  errors.
  (`MR84 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/84>`__)
- Breaking: ProcessingBlock factory methods simplified
  (`MR82 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/82>`__)

0.11.0
------

- Script reports application level errors to pb state
  (`MR80 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/80>`__)
- Processing block parameter validation returns parameter class
  (`MR81 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/81>`__)

0.10.0
------

- Update ska-sdp-config to 0.10.0
  (`MR79 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/79>`__)
- Phase object monitors data flow states for application errors
  (`MR78 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/78>`__)
- Add processing block parameter validation based on Pydantic
  (`MR77 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/77>`__)
- Update ska-sdp-config to 0.9.1
  (`MR76 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/76>`__)
- Update deployment monitoring in ProcessingBlock to comply with new
  deployment state structure and report and aggregate deployment errors
  (`MR72 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/72>`__)

0.9.0
-----

- Use version 0.9.0 of Configuration Library, which provides a simple
  FlowSource model
  (`MR74 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/74>`__)
- Implement adding sources to DataFlow objects
  (`MR74 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/74>`__)

0.8.0
-----

- Use version 0.8.0 of the configuration library which adds a pydantic
  model for the execution block
  (`MR73 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/73>`__)
- Use version 0.7.0 of the configuration library which uses pydantic
  (`MR71 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/71>`__)
- Removed deprecated config lib txn methods
  (`MR68 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/68>`__)

0.7.1
-----

- Bugfix: Phase.\__exit\_\_ correctly cleans up CANCELLED deployments
  (`MR66 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/66>`__)

0.7.0
-----

- Add data flow entries, compliance with latest config db
  (`MR64 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/64>`__)
- Factored out version.py
  (`MR61 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/61>`__)
- Simplified the receive address code and added comments to make it more
  understandable
  (`MR60 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/60>`__)
- Updated the library to use correct version of setting hosts and ports
  in receive addresses
  (`MR59 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/59>`__)
- Fix resolving default beam info in receive addresses and scan types
  (`MR58 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/58>`__)
- Added ska-rt step to CI pipeline
  (`MR53 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/53>`__)

0.6.3
-----

- Add queue connector configuration code to phase
  (`MR52 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/52>`__)

0.6.2
-----

- Only generate hosts and ports in receive addresses for receive script
  (`MR51 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/51>`__)
- Do not raise an error when EB is cancelled (only log error,
  `MR49 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/49>`__)
- Add skart.toml file
  (`MR50 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/50>`__)
- Get processing block dependencies via ProcessingBlock object
  (`MR48 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/48>`__)

0.6.1
-----

- Updated to use the new version of config DB 0.5.3

0.6.0
-----

- Updated to use the new version of config DB 0.5.2
  (`MR46 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/46>`__)

0.5.2
-----

- Option to specify how much time to wait before deployments_ready is
  set to True. This is a feature only of FakeDeployments for testing
  (`MR44 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/44>`__)
- How FakeDeployments deal with setting the deployments_ready key in the
  processing block state
  (`MR44 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/44>`__)

0.5.1
-----

- Account for scripts that do not deploy anything in the deployment
  monitoring code
  (`MR43 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/43>`__)
- Fix ``Phase.ee_deploy_test`` (``fake_deploy``)
  (`MR43 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/43>`__)

0.5.0
-----

- Monitor deployments as part of Phase.wait_loop. Update the processing
  block state with an aggregate deployment sate key: deployments_ready
  (`MR39 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/39>`__,
  `MR41 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/41>`__,
  `MR42 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/42>`__)
- Use config.watcher instead of txn.loop
  (`MR40 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/40>`__)
- Bug fix in setting hosts (ORC-1436)
  (`MR37 <https://gitlab.com/ska-telescope/sdp/ska-sdp-scripting/-/merge_requests/37>`__)

0.4.2
-----

- Remove hard-coded cluster domain name in receive addresses.

0.2.6
-----

- Updated with functionalities to support generic receive chart (0.2.0)

0.2.5
-----

- Publish Python package in central artefact repository.

0.2.4
-----

- Bug fixed. Fixed boolean flags in the chart values. Updated to use
  workflow docker image itself for the scheduler and workers to
  guarantee that the dask and distributed versions will match.

0.2.3
-----

- Updated with the missing functionalities for multiple receive process
  and port matching.

0.2.2
-----

- Added functions to deploy multiple receive processes. The ports
  matches the actual ports the receive processes are using.
